package com.pixgrill.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.view.View
import com.pixgrill.R
import android.widget.*


/**
 * Created by Developer on 12/15/2017.
 */
fun Context.Toast(message: CharSequence, length: Int = Toast.LENGTH_LONG) = Toast.makeText(this, message, length).show()

inline fun <reified T : Activity> Activity.navigate(bundle: Bundle) {
    val intent = Intent(this, T::class.java).putExtras(bundle)
    startActivity(intent)
}


fun imageUrlCheck(path: String?): String? {
    if (path != null && !path?.equals("")) {
        var subStr = path?.substring(0, 4)
        if (subStr.equals("www.")) {
            return path?.replaceFirst(subStr!!, "http://");
        }
    }
    return path
}

inline fun <T, R : Comparable<R>> Sequence<T>.sortedByDirection(
        descending: Boolean,
        crossinline selector: (T) -> R?
): Sequence<T> {
    val comparator = if (descending) {
        compareByDescending(selector)
    } else {
        compareBy(selector)
    }

    return sortedWith(comparator)
}

fun <T> Sequence<T>.batch(n: Int): Sequence<MutableList<T>> {
    return BatchingSequence(this, n)
}

fun <T> Sequence<T>.batchWhile(batchSize: Int, predicate: (MutableList<T>) -> Boolean): Sequence<MutableList<T>> {
    return this.batch(batchSize)
            .takeWhile(predicate = predicate)
}

private class BatchingSequence<T>(val source: Sequence<T>, val batchSize: Int) : Sequence<MutableList<T>> {
    override fun iterator(): Iterator<MutableList<T>> = object : AbstractIterator<MutableList<T>>() {
        val iterate = if (batchSize > 0) source.iterator() else emptyList<T>().iterator()
        override fun computeNext() {
            if (iterate.hasNext()) setNext(iterate.asSequence().take(batchSize).toMutableList())
            else done()
        }
    }
}

fun showSnackBarError(context: Context, message: String, length: Int): Snackbar? {
    var snackbar: Snackbar? = null
    try {
        snackbar = Snackbar.make((context as Activity).findViewById(android.R.id.content),
                message, length)
        val view = snackbar.view
        view.setBackgroundColor(Color.parseColor("#d8453c"))
        val textView = view
                .findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        textView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        textView.setBackgroundColor(Color.parseColor("#d8453c"))
        textView.typeface = ResourcesCompat.getFont(context, R.font.helvetica_neue_roman)
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return snackbar
}


