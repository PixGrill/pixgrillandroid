package com.pixgrill.util

import android.app.ProgressDialog
import android.content.Context

class ProgressUtils(context: Context) {

    private val _progressDialog: ProgressDialog?

    init {
        _progressDialog = ProgressDialog(context)
    }

    fun showProgress(message: String) {
        if (_progressDialog != null && !_progressDialog.isShowing) {
            _progressDialog.setMessage(message)
            _progressDialog.setCancelable(false)
            _progressDialog.show()
        }
    }

    fun dismissProgress() {
        if (_progressDialog != null && _progressDialog.isShowing) {
            _progressDialog.dismiss()
        }
    }
}
