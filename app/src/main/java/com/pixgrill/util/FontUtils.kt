package com.sportsdb.util

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Zoondia on 11/3/2016.
 */

object FontUtils {

    private val FONT_LIGHT = "font/roboto_light.ttf"
    private val FONT_MEDIUM = "font/roboto_medium.ttf"
    private val FONT_REGULAR = "font/roboto_regular.ttf"

    fun getCustomTypeFace(context: Context, fontType: Int): Typeface {
        val typeface: Typeface
        if (fontType == FontTypeIntDef.REGULAR) {
            typeface = Typeface.createFromAsset(context.assets,
                    FONT_REGULAR)
        } else if (fontType == FontTypeIntDef.LIGHT) {
            typeface = Typeface.createFromAsset(context.assets,
                    FONT_LIGHT)
        } else {
            typeface = Typeface.createFromAsset(context.assets,
                    FONT_MEDIUM)
        }
        return typeface
    }


}
