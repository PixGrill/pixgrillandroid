package com.pixgrill.util

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics

/**
 * Created by Android : BizClips on 8/21/2017.
 */
class DisplayUtils(context: Context) {
    var screenWidth: Int = 0
        private set
    var screenHeight: Int = 0
        private set

    init {
        val displaymetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displaymetrics)
        screenWidth = displaymetrics.widthPixels
        screenHeight = displaymetrics.heightPixels
    }


    fun getHeightPercentage(percentage: Int): Int {
        return screenHeight * percentage / 100
    }

    fun getWidthPercentage(percentage: Int): Int {
        return screenWidth * percentage / 100
    }
}
