package com.sportsdb.util

import android.support.annotation.IntDef
import java.lang.annotation.RetentionPolicy


object FontTypeIntDef {
    val REGULAR = 1
    val MEDIUM = 2
    val LIGHT = 3
}