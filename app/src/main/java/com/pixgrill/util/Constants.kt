package com.pixgrill.util

import com.amazonaws.regions.Regions



/**
 * Created by Developer on 27-12-2017.
 */
object Constants {

    /*Arguments*/
    val ARGS_LEAGUE_ID = "leagueID"
    val ARGS_LEAGUE_MODEL = "leaguemodel"
    val ARGS_TEAM_MODEL = "teammodel"
    val ARGS_PLAYER_MODEL = "playermodel"
    val ARGS_SEASON_MODEL = "seasonmodel"
    val ARGS_FULL_SCREEN_IMAGE = "fullScreenImage"
    val ARGS_EVENT_MODEL = "eventmodel"

    val ARGS_NOT_AVAILABLE = "Not Available"
    val ARGS_FAVOURITE_LIST = "favouriteList"
    val TEXT_NO_RESULT_FOUND = "No result found for "

    val ARGS_PAGE_SIZE = 15

    val FALSE: Int = 0
    val TRUE: Int = 1




    /**
     * Bucket Name
     */

    /*
     * Region of your bucket.
     */
    val BUCKET_REGION = Regions.US_EAST_1

    val PROFILE_PATH = "profile_pictures/"

    val PROFILE_PATH_EXTENSION = ".jpg"

    val FEED_PATH = "feeds/"
    val WELCOME_PATH = "welcome"


}