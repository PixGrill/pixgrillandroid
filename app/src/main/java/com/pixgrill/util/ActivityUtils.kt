package com.pixgrill.util

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.pixgrill.util.Preconditions.checkNotNull


/**
 * This provides methods to help Activities load their UI.
 */
object ActivityUtils {

    /**
     * The `fragment` is added to the container view with id `frameId`. The operation is
     * performed by the `fragmentManager`.
     */
    fun addFragmentToActivity(fragmentManager: FragmentManager,
                              fragment: Fragment, frameId: Int) {
        checkNotNull(fragmentManager)
        checkNotNull(fragment)
        val transaction = fragmentManager.beginTransaction()
        transaction.add(frameId, fragment)
        transaction.addToBackStack("")
        transaction.commit()
    }

    /**
     * The `fragment` is added to the container view with id `frameId`. The operation is
     * performed by the `fragmentManager`.
     */
    fun replaceFragmentToActivity(fragmentManager: FragmentManager,
                                  fragment: Fragment, frameId: Int) {
        checkNotNull(fragmentManager)
        checkNotNull(fragment)
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(frameId, fragment)
        transaction.commit()
    }

    /**
     * @param activity
     * @param aClass
     * @param bundle
     * @param isFinish
     */
    fun startActivity(activity: Activity, aClass: Class<*>, bundle: Bundle?, isFinish: Boolean) {
        val intent = Intent(activity, aClass)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        activity.startActivity(intent)
        if (isFinish) {
            activity.finish()
        }
    }

    /**
     * @param activity
     * @param aClass
     * @param bundle
     * @param isFinish
     */
    fun startActivityForResult(activity: Activity, aClass: Class<*>, bundle: Bundle?, isFinish: Boolean, requestCode: Int) {
        val intent = Intent(activity, aClass)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        activity.startActivityForResult(intent, requestCode)
        if (isFinish) {
            activity.finish()
        }
    }
}
