package com.pixgrill.model


class UserRequestModel {
    var type: String? = null
    var nickName: String? = null
    var email: String? = null
    var password: String? = null
    var profileImageUrl: String? = null
    var categoryId: Int? = null
    var cameraSpec: String? = null
    var dob: String? = null
    var gender: String? = null
    var mobileNumber: String? = null
    var bio: String? = null
    var facebookUserName: String? = null
    var twitterUserName: String? = null
    var accountId: String? = null

}