package com.pixgrill.model;


import android.os.Parcel;
import android.os.Parcelable;

public class DrawerListModel implements Parcelable {

    public int drawerIcon;
    public String drawerName;


    public DrawerListModel(int id, String drawerName) {
        this.drawerIcon = id;
        this.drawerName = drawerName;
    }


    public int getId() {
        return drawerIcon;
    }

    public String getName() {
        return drawerName;
    }


    protected DrawerListModel(Parcel in) {
        drawerIcon = in.readInt();
        drawerName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(drawerIcon);
        dest.writeString(drawerName);
    }

    @SuppressWarnings("unused")
    public static final Creator<DrawerListModel> CREATOR = new Creator<DrawerListModel>() {
        @Override
        public DrawerListModel createFromParcel(Parcel in) {
            return new DrawerListModel(in);
        }

        @Override
        public DrawerListModel[] newArray(int size) {
            return new DrawerListModel[size];
        }
    };
}
