package com.pixgrill.model

class NotificationModel {
    var notificationId: Int? = null
    var nickName: String? = null
    var content: String? = null
    var image: String? = null
    var time: String? = null
}