package com.pixgrill.model

class CommentModel {
    var commentId: Int? = null
    var nickName: String? = null
    var timeInHorurs: String? = null
    var comment: String? = null
}