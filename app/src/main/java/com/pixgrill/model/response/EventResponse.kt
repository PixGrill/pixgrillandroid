package com.pixgrill.model.response

import java.io.Serializable

class EventResponse (
        var eventId: Int? = null,
        var eventName: String? = "",
        var eventImage: String? = "",
        var eventStatus: Int? = 0
):Serializable {


}