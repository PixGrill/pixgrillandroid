package com.pixgrill.model.response


data class UserResponse(
    val twitterUserName: String,
    val gender: String,
    val nickName: String,
    val mobileNumber: String,
    val bio: String,
    val userId: Int,
    val cameraSpec: String,
    val dob: String,
    val facebookUserName: String,
    val category: String,
    val auth_token: String,
    val profileImageUrl: String,
    val email: String
)