package com.pixgrill.model;


import android.os.Parcel;
import android.os.Parcelable;

public class SheetListModel implements Parcelable {

    private int typeId;
    private String typeName;


    public SheetListModel(int id, String itemName) {
        this.typeId = id;
        this.typeName = itemName;
    }


    public int getId() {
        return typeId;
    }

    public String getName() {
        return typeName;
    }


    protected SheetListModel(Parcel in) {
        typeId = in.readInt();
        typeName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeId);
        dest.writeString(typeName);
    }

    @SuppressWarnings("unused")
    public static final Creator<SheetListModel> CREATOR = new Creator<SheetListModel>() {
        @Override
        public SheetListModel createFromParcel(Parcel in) {
            return new SheetListModel(in);
        }

        @Override
        public SheetListModel[] newArray(int size) {
            return new SheetListModel[size];
        }
    };
}
