package com.pixgrill.ui.activity.login

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.facebook.*
import com.facebook.login.LoginResult
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.model.response.UserResponse
import com.pixgrill.ui.activity.home.HomeActivity
import com.pixgrill.ui.activity.registration.RegistrationActivity
import com.pixgrill.util.*
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Result
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.util.*


class LoginActivity : BaseActivity(), LoginContract.View {

    var callbackManager: CallbackManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        LoginPresenter(this)

    }


    override fun onCreateViews(displayUtils: DisplayUtils) {
        super.onCreateViews(displayUtils)
    }

    override fun onViewCreated() {
        super.onViewCreated()
        signUpContainer.setOnClickListener{
            ActivityUtils.startActivity(this, RegistrationActivity::class.java, null, false)
        }

        loginButton.setOnClickListener{
            mPresenter?.userLogin(editEmail.text.toString(), editPassword.text.toString(), "0")
        }

        loginWithTwitter.callback = object : Callback<TwitterSession>(){
            override fun success(result: Result<TwitterSession>?) {
                val session = result?.data
                mPresenter?.userLoginWithTwitterOrFb(session?.userName!!, "2")
            }

            override fun failure(exception: TwitterException?) {
            }
        };
        callbackManager = CallbackManager.Factory.create();

        loginWithFacebook.setReadPermissions(Arrays.asList("email"));
        loginWithFacebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult?) {
                var userId: String? = ""
                var request  = GraphRequest.newMeRequest(result?.accessToken, object: GraphRequest.GraphJSONObjectCallback{
                    override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                        try{
                            userId = `object`?.getString("email")
                        }catch (e: Exception){
                            e.printStackTrace();
                        }
                    }

                })
                mPresenter?.userLoginWithTwitterOrFb(userId!!, "1")
            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException?) {
            }

        })

        fbLoginButton.setOnClickListener {
            loginWithFacebook.performClick()
        }

        twitterLoginButton.setOnClickListener {
            loginWithTwitter.performClick()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        loginWithTwitter.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data);
    }

    override fun onClickListener(view: View?) {

    }
    private var mPresenter: LoginContract.Presenter? = null


    override fun setPresenter(presenter: LoginContract.Presenter?) {
        mPresenter = Preconditions.checkNotNull(presenter)
    }

    override fun onRequestStart() {
        progressView.visibility = View.VISIBLE
    }

    override fun onRequestStop() {
        progressView.visibility = View.INVISIBLE
    }

    override fun onRequestError(message: String?) {
        Toast(message!!)
    }

    override fun onCriticalError() {

    }

    override fun onRequestSuccess(response: UserResponse?) {
        if(response != null){
            _preferenceData.userDetails = response
            _preferenceData.refreshToken = response.auth_token
            ActivityUtils.startActivity(this, HomeActivity::class.java, null, true)
        }
    }

    override fun onEmailAddressError(type: Int) {
        if (type == GlobalTypeIntDef.EMPTY_ERROR) {
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_email_empty_error), getString(R.string.text_email_empty_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
        if (type == GlobalTypeIntDef.VALIDATION_ERROR){
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_error_format), getString(R.string.text_email_validity_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
    }

    override fun onPasswordError(type: Int) {
        if (type == GlobalTypeIntDef.EMPTY_ERROR){
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_error_format), getString(R.string.text_password_empty_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.cancelRequest()
    }
}
