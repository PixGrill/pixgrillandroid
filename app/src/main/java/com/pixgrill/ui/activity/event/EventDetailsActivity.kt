package com.pixgrill.ui.activity.event

import android.os.Bundle
import android.view.View
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.model.response.EventResponse
import com.pixgrill.ui.fragment.events.live.LiveFragment
import com.pixgrill.ui.fragment.events.upcoming.UpComingEventFragment
import com.pixgrill.util.ActivityUtils
import com.pixgrill.util.Constants
import kotlinx.android.synthetic.main.activity_event_details.*

class EventDetailsActivity : BaseActivity() {

    var _model: EventResponse? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = intent.extras
        if (bundle != null) {
            _model = bundle.getSerializable(Constants.ARGS_EVENT_MODEL) as EventResponse
        }
        setContentView(R.layout.activity_event_details)
    }

    override fun onViewCreated() {
        super.onViewCreated()
        toolbarBack.setOnClickListener { finish() }
        if (_model?.eventStatus == 1) {
            ActivityUtils.replaceFragmentToActivity(supportFragmentManager, LiveFragment.newInstance(), R.id.fragmentContainer)
        } else if (_model?.eventStatus == 2) {
            ActivityUtils.replaceFragmentToActivity(supportFragmentManager, UpComingEventFragment.newInstance(), R.id.fragmentContainer)
        }

    }

    override fun onClickListener(view: View?) {

    }
}
