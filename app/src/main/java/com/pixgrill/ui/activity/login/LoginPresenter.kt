package com.pixgrill.ui.activity.login

import android.content.Context
import android.text.TextUtils
import com.pixgrill.base.ResponseBase
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.data.Params.*
import com.pixgrill.model.UserRequestModel
import com.pixgrill.model.response.UserResponse
import com.pixgrill.network.ErrorHandlingAdapter
import com.pixgrill.network.RequestCall
import com.pixgrill.network.RequestHelper
import com.pixgrill.network.utils.RequestParams
import com.pixgrill.util.Preconditions
import com.pixgrill.util.isValidEmail
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException


class LoginPresenter internal constructor(view: LoginContract.View) : LoginContract.Presenter, ErrorHandlingAdapter.RetroCallback<ResponseBase<UserResponse>> {

    var call: ErrorHandlingAdapter.RetroCall<ResponseBase<UserResponse>>? = null
    private val mView: LoginContract.View = Preconditions.checkNotNull<LoginContract.View>(view, view.javaClass.simpleName)

    init {
        mView.setPresenter(this)
    }


    override fun start(context: Context?) {

    }

    override fun userLogin(email: String, password: String, type: String?) {
        if (TextUtils.isEmpty(email)) {
            mView.onEmailAddressError(GlobalTypeIntDef.EMPTY_ERROR)
        } else if (!isValidEmail(email)) {
            mView.onEmailAddressError(GlobalTypeIntDef.VALIDATION_ERROR)
        } else if (TextUtils.isEmpty(password)) {
            mView.onPasswordError(GlobalTypeIntDef.EMPTY_ERROR)
        } else {
            val userRequest = UserRequestModel()
            userRequest.email = email
            userRequest.password = password
            userRequest.type = type
            val requestCall = RequestHelper.createRequest(RequestCall.ILogin::class.java, null)
            call = requestCall.login(userRequest)
            call!!.enqueue(this)
        }
    }

    override fun userLoginWithTwitterOrFb(accountId: String, type: String?) {
        if(TextUtils.isEmpty(accountId)){
            mView.onPasswordError(GlobalTypeIntDef.EMPTY_ERROR)
        }else{
            val userRequest = UserRequestModel()
            userRequest.accountId = accountId
            userRequest.type = type
            val requestCall = RequestHelper.createRequest(RequestCall.ILogin::class.java, null)
            call = requestCall.login(userRequest)
            call!!.enqueue(this)
        }
    }

    override fun onRequestStart() {
        mView.onRequestStart()
    }

    override fun onRequestStop() {
        mView.onRequestStop()
    }

    override fun onSuccess(response: Response<ResponseBase<UserResponse>>) {
        val model = response.body()
        if (model != null) {
            if (model.isHasError) {
                mView.onRequestError(model.message)
            } else {
                mView.onRequestSuccess(model.response)
            }
        } else {
            mView.onCriticalError()
        }
    }

    override fun onUnauthenticated(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onClientError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onServerError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onNetworkError(e: IOException) {
        mView.onRequestError(e.message)
    }

    override fun onUnexpectedError(t: Throwable) {
        mView.onRequestError(t.message)
    }

    override fun cancelRequest() {
        if (call != null) {
            call!!.cancel()
        }
    }

}
