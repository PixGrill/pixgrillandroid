package com.pixgrill.ui.activity.createpost

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.ui.fragment.userprofile.editprofile.EditProfileFragment
import com.pixgrill.ui.fragment.userprofile.ownprofile.OwnProfileFragment
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.include_toolbar_layout.*

class CreatePostActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_post)
        toolbarTitle.text = getString(R.string.title_my_profile);
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if(supportFragmentManager!=null&&supportFragmentManager.fragments!=null){

            if(supportFragmentManager.fragments.get(0) is OwnProfileFragment){
                toolbarEdit.visibility = View.VISIBLE
                toolbarTitle.text = getString(R.string.title_my_profile);
            }

        }
    }

    override fun onClickListener(view: View?) {

    }
}
