package com.pixgrill.ui.activity.friends

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.pixgrill.R
import com.pixgrill.ui.fragment.userprofile.editprofile.EditProfileFragment
import com.pixgrill.ui.fragment.userprofile.ownprofile.OwnProfileFragment
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.include_toolbar_layout.*

class FriendsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)

        toolbarTitle.text = getString(R.string.title_my_profile);

        ActivityUtils.replaceFragmentToActivity(supportFragmentManager,
                OwnProfileFragment.newInstance(), R.id.fragmentContainer)

        toolbarEdit.setOnClickListener{
            ActivityUtils.addFragmentToActivity(supportFragmentManager,
                    EditProfileFragment.newInstance(), R.id.fragmentContainer)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if(supportFragmentManager!=null&&supportFragmentManager.fragments!=null){

            if(supportFragmentManager.fragments.get(0) is OwnProfileFragment){
                toolbarEdit.visibility = View.VISIBLE
                toolbarTitle.text = getString(R.string.title_my_profile);
            }

        }
    }
}
