package com.pixgrill.ui.activity.splash


import com.pixgrill.base.BaseRequestPresenter
import com.pixgrill.base.BaseRequestView
import com.pixgrill.model.response.AppDataResponse

interface SplashScreenContract {

    interface View : BaseRequestView<AppDataResponse, Presenter> {

        fun navigateToLoginPage()

        fun navigateToHomePage()


    }

    interface Presenter : BaseRequestPresenter {
        fun requestAppDataService(userID: Int,token: String?)


    }
}
