package com.pixgrill.ui.activity.addPost

import android.content.Context
import com.pixgrill.base.ResponseBase
import com.pixgrill.model.response.UserResponse
import com.pixgrill.network.ErrorHandlingAdapter
import com.pixgrill.util.Preconditions
import retrofit2.Response
import java.io.IOException

class AddPostPresenter internal constructor(view: AddPostContract.View): AddPostContract.Presenter, ErrorHandlingAdapter.RetroCallback<ResponseBase<UserResponse>> {
    var call: ErrorHandlingAdapter.RetroCall<ResponseBase<UserResponse>>? = null
    private val mView: AddPostContract.View = Preconditions.checkNotNull<AddPostContract.View>(view, view.javaClass.simpleName)

    init {
        mView.setPresenter(this)
    }

    override fun addPost() {
    }

    override fun start(context: Context?) {
    }

    override fun cancelRequest() {
        if (call != null) {
            call!!.cancel()
        }
    }

    override fun onRequestStart() {
        mView.onRequestStart()
    }

    override fun onRequestStop() {
        mView.onRequestStop()
    }

    override fun onSuccess(response: Response<ResponseBase<UserResponse>>) {
        val model = response.body()
        if (model != null) {
            if (model.isHasError) {
                mView.onRequestError(model.message)
            } else {
                mView.onRequestSuccess(model.response)
            }
        } else {
            mView.onCriticalError()
        }
    }

    override fun onUnauthenticated(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onClientError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onServerError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onNetworkError(e: IOException) {
        mView.onRequestError(e.message)
    }

    override fun onUnexpectedError(t: Throwable) {
        mView.onRequestError(t.message)
    }
}