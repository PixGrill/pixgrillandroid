package com.pixgrill.ui.activity.registration

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.model.response.UserResponse
import com.pixgrill.ui.activity.login.LoginActivity
import com.pixgrill.ui.activity.profile.ProfileActivity
import com.pixgrill.util.*
import kotlinx.android.synthetic.main.activity_registration.*


class RegistrationActivity : BaseActivity(), RegistrationContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        RegistrationPresenter(this)
    }


    override fun onCreateViews(displayUtils: DisplayUtils) {
        super.onCreateViews(displayUtils)
    }

    override fun onViewCreated() {
        super.onViewCreated()
        signInContainer.setOnClickListener{
            ActivityUtils.startActivity(this, LoginActivity::class.java, null, true)

        }
        btnSignUp.setOnClickListener{
            mPresenter?.userSignUp(editNickName.text.toString(), editEmail.text.toString(), editPassword.text.toString())
        }
    }
    override fun onClickListener(view: View?) {

    }
    private var mPresenter: RegistrationContract.Presenter? = null


    override fun setPresenter(presenter: RegistrationContract.Presenter?) {
        mPresenter = Preconditions.checkNotNull(presenter)
    }

    override fun onRequestStart() {
        progressView.visibility = View.VISIBLE
    }

    override fun onRequestStop() {
        progressView.visibility = View.INVISIBLE
    }

    override fun onRequestError(message: String?) {
        Toast(message!!)
    }

    override fun onCriticalError() {

    }

    override fun onRequestSuccess(response: UserResponse?) {
        if(response != null){
            _preferenceData.userDetails = response
            ActivityUtils.startActivity(this, ProfileActivity::class.java, null, true)
        }
    }

    override fun onNickNameError(type: Int) {
        if (type == GlobalTypeIntDef.EMPTY_ERROR) {
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_nickname_empty_error), getString(R.string.text_nickname_empty_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
    }

    override fun onEmailAddressError(type: Int) {
        if (type == GlobalTypeIntDef.EMPTY_ERROR) {
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_email_empty_error), getString(R.string.text_email_empty_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
        if (type == GlobalTypeIntDef.VALIDATION_ERROR){
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_error_format), getString(R.string.text_email_validity_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
    }

    override fun onPasswordError(type: Int) {
        if (type == GlobalTypeIntDef.EMPTY_ERROR){
            showSnackBarError(
                    this,
                    String.format(getString(R.string.text_error_format), getString(R.string.text_password_empty_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.cancelRequest()
    }
}
