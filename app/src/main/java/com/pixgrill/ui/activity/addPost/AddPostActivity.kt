package com.pixgrill.ui.activity.addPost

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.facebook.drawee.view.SimpleDraweeView
import com.pixgrill.R
import com.pixgrill.amazon.AmazonUtils
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.StaticData
import com.pixgrill.dialog.SheetFragment
import com.pixgrill.model.SheetListModel
import kotlinx.android.synthetic.main.content_add_post.*
import kotlinx.android.synthetic.main.include_toolbar_layout.*
import java.io.File
import java.util.*

class AddPostActivity : BaseActivity(), SheetFragment.Listener {

    val PICK_IMAGE = 1
    private val SHEET_DIALOG_ID_PHOTO = 1002
    lateinit var imageUris: MutableList<Uri>
    var videoTransferId = -1
    var _fileName = UUID.randomUUID().toString()
    private var _transferUtility: TransferUtility? = null
    var uploadingImageOne = false
    var uploadingImageTwo = false
    var uploadingImageThree = false
    var uploadingImageFour = false
    var uploadingInProgress = false

//    https://s3.ap-south-1.amazonaws.com/pixgrill/postUploads/14/filename

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)
        toolbarTitle.text = getString(R.string.title_create_post);
        imageUris = mutableListOf()
        _transferUtility = AmazonUtils.getTransferUtility(this);

    }

    override fun onViewCreated() {
        super.onViewCreated()
        toolbarBack.setOnClickListener {
            finish()
        }


        textViewPlaceHolderOne.setOnClickListener {
            if (!uploadingInProgress) {
                uploadingImageOne = true
                galleryIntent(galleryImageOne)
            }
        }
        textViewPlaceHolderTwo.setOnClickListener {
            if (!uploadingInProgress) {
                uploadingImageTwo = true
                galleryIntent(galleryImageOne)
            }
        }
        textViewPlaceHolderThree.setOnClickListener {
            if (!uploadingInProgress) {
                uploadingImageThree = true
                galleryIntent(galleryImageOne)
            }
        }
        textViewPlaceHolderFour.setOnClickListener {
            if (!uploadingInProgress) {
                uploadingImageFour = true
                galleryIntent(galleryImageOne)
            }
        }

        cancelImageOne.setOnClickListener {
            imageUris.removeAt(0)
            setImageInView(imageUris.size)
        }
        cancelImageTwo.setOnClickListener {
            imageUris.removeAt(1)
            setImageInView(imageUris.size)
        }
        cancelImageThree.setOnClickListener {
            imageUris.removeAt(2)
            setImageInView(imageUris.size)
        }
        cancelImageFour.setOnClickListener {
            imageUris.removeAt(3)
            setImageInView(imageUris.size)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            var f: File? = null
            var uri: Uri? = null
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                // or get a single image only
                val image = ImagePicker.getFirstImageOrNull(data)
                f = File(image.path)
                uri = Uri.fromFile(f)
            } else if (resultCode === Activity.RESULT_OK) {
                uri = data?.data
                val path = getPathFromURI(uri!!)
                if (path != null) {
                    val f = File(path)
                    uri = Uri.fromFile(f)
                }
            }


            if (imageUris.size < 4) {
                imageUris.add(uri!!)
                setImageInView(imageUris.size)
            }
            //uploadWithTransferUtility(s3credentialsProvider,File(uri.toString()))
            if (f != null) {
                uploadFileToS3(f)
            }
        } catch (e: Exception) {
            e.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    fun getPathFromURI(contentUri: Uri): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(contentUri, proj, null, null, null)
        if (cursor!!.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    override fun onClickListener(view: View?) {

    }


    private fun uploadFileToS3(file: File) {
        //val file = File(fileString)
        if(uploadingImageOne){
            progressBar.visibility = View.VISIBLE
        }else if(uploadingImageTwo){
            progressBar2.visibility = View.VISIBLE
        }else if(uploadingImageThree){
            progressBar3.visibility = View.VISIBLE
        }else if(uploadingImageFour){
            progressBar4.visibility = View.VISIBLE
        }
        uploadingInProgress = true
        if (videoTransferId !== -1) {
            _transferUtility?.cancel(videoTransferId)
        }
        var fileNameForUpload = System.currentTimeMillis().toString()
        val observer = _transferUtility?.upload(AmazonUtils.BUCKET_NAME,
                String.format(getString(R.string.text_amazon_s3_feed_upload_url), (_preferenceData.userDetails.userId.toString() + "/" + fileNameForUpload)),
                file, CannedAccessControlList.PublicRead)
        videoTransferId = observer?.getId()!!
        observer?.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                println("id = [${id}], state = [${state}]")
                if (state == TransferState.COMPLETED) {
                    uploadingInProgress = false
                    if (uploadingImageOne) {
                        uploadingImageOne = false
                        cancelImageOne.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                    } else if (uploadingImageTwo) {
                        uploadingImageTwo = false
                        cancelImageTwo.visibility = View.VISIBLE
                        progressBar2.visibility = View.GONE
                    } else if (uploadingImageThree) {
                        uploadingImageThree = false
                        cancelImageThree.visibility = View.VISIBLE
                        progressBar3.visibility = View.GONE
                    } else if (uploadingImageFour) {
                        uploadingImageFour = false
                        progressBar4.visibility = View.GONE
                        cancelImageFour.visibility = View.VISIBLE
                    }
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                println("id = [${id}], bytesCurrent = [${bytesCurrent}], bytesTotal = [${bytesTotal}]")
            }

            override fun onError(id: Int, ex: Exception) {
                println("id = [${id}], ex = [${ex}]")
                ex.printStackTrace()

            }
        })
    }

    fun setImageInView(listSize: Int) {
        if (listSize == 1) {
            galleryImageOne.setImageURI(imageUris[0].toString())
            cancelImageOne.visibility = View.VISIBLE
            cancelImageTwo.visibility = View.GONE
            cancelImageThree.visibility = View.GONE
            cancelImageFour.visibility = View.GONE

            textViewPlaceHolderOne.visibility = View.GONE
            textViewPlaceHolderTwo.visibility = View.VISIBLE
            textViewPlaceHolderThree.visibility = View.VISIBLE
            textViewPlaceHolderFour.visibility = View.VISIBLE
        } else if (listSize == 2) {
            galleryImageOne.setImageURI(imageUris[0].toString())
            galleryImageTwo.setImageURI(imageUris[1].toString())
            cancelImageOne.visibility = View.VISIBLE
            cancelImageTwo.visibility = View.VISIBLE
            cancelImageThree.visibility = View.GONE
            cancelImageFour.visibility = View.GONE

            textViewPlaceHolderOne.visibility = View.GONE
            textViewPlaceHolderTwo.visibility = View.GONE
            textViewPlaceHolderThree.visibility = View.VISIBLE
            textViewPlaceHolderFour.visibility = View.VISIBLE
        } else if (listSize == 3) {
            galleryImageOne.setImageURI(imageUris[0].toString())
            galleryImageTwo.setImageURI(imageUris[1].toString())
            galleryImageThree.setImageURI(imageUris[2].toString())
            cancelImageOne.visibility = View.VISIBLE
            cancelImageTwo.visibility = View.VISIBLE
            cancelImageThree.visibility = View.VISIBLE
            cancelImageFour.visibility = View.GONE

            textViewPlaceHolderOne.visibility = View.GONE
            textViewPlaceHolderTwo.visibility = View.GONE
            textViewPlaceHolderThree.visibility = View.GONE
            textViewPlaceHolderFour.visibility = View.VISIBLE
        } else if (listSize == 4) {
            galleryImageOne.setImageURI(imageUris[0].toString())
            galleryImageTwo.setImageURI(imageUris[1].toString())
            galleryImageThree.setImageURI(imageUris[2].toString())
            galleryImageFour.setImageURI(imageUris[3].toString())
            cancelImageOne.visibility = View.VISIBLE
            cancelImageTwo.visibility = View.VISIBLE
            cancelImageThree.visibility = View.VISIBLE
            cancelImageFour.visibility = View.VISIBLE
            textViewPlaceHolderOne.visibility = View.GONE
            textViewPlaceHolderTwo.visibility = View.GONE
            textViewPlaceHolderThree.visibility = View.GONE
            textViewPlaceHolderFour.visibility = View.GONE
        } else if (listSize == 0) {

            textViewPlaceHolderOne.visibility = View.VISIBLE
            textViewPlaceHolderTwo.visibility = View.VISIBLE
            textViewPlaceHolderThree.visibility = View.VISIBLE
            textViewPlaceHolderFour.visibility = View.VISIBLE

            cancelImageOne.visibility = View.GONE
            cancelImageTwo.visibility = View.GONE
            cancelImageThree.visibility = View.GONE
            cancelImageFour.visibility = View.GONE
        }
    }

    fun galleryIntent(image: SimpleDraweeView) {

        val sheetFragment = SheetFragment.newInstance(SHEET_DIALOG_ID_PHOTO, StaticData.getCameraPickerList(), "CHOOSE")
        sheetFragment.show(this.supportFragmentManager, "Photo")
    }

    override fun itemStateSelected(sheetModel: Any?, dialogId: Int) {
        val model = (sheetModel as SheetListModel)
        if (dialogId == SHEET_DIALOG_ID_PHOTO) {
            if (model.id == 0) {
                ImagePicker.create(this)
                        .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                        .includeVideo(true) // Show video on image picker
                        .single() // single mode
                        .showCamera(false) // show camera or not (true by default)
                        .start() // start image picker activity with request code
            } else {
                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, 1)
            }
        }
    }
}
