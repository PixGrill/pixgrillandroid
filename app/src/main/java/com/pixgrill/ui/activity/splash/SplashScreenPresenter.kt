package com.pixgrill.seven.ui.activity.splash

import android.content.Context
import android.support.v4.util.Preconditions
import com.pixgrill.network.RequestHelper
import com.pixgrill.base.ResponseBase
import com.pixgrill.data.Params
import com.pixgrill.model.response.AppDataResponse
import com.pixgrill.network.ErrorHandlingAdapter
import com.pixgrill.network.RequestCall
import com.pixgrill.network.utils.RequestParams
import com.pixgrill.ui.activity.splash.SplashScreenContract
import retrofit2.Response
import java.io.IOException


class SplashScreenPresenter internal constructor(view: SplashScreenContract.View) : SplashScreenContract.Presenter, ErrorHandlingAdapter.RetroCallback<ResponseBase<AppDataResponse>> {

    var call: ErrorHandlingAdapter.RetroCall<ResponseBase<AppDataResponse>>? = null

    override fun cancelRequest() {
        if (call != null) {
            call!!.cancel()
        }
    }


    private val mView: SplashScreenContract.View = Preconditions.checkNotNull<SplashScreenContract.View>(view, view.javaClass.simpleName)

    init {
        mView.setPresenter(this)
    }

    override fun start(context: Context) {
    }

    override fun requestAppDataService(userID: Int, token: String?) {
        val params = RequestParams()
        params.put(Params.PARAM_USER_ID, userID)
        params.put(Params.PARAM_DEVICE_TOKEN, token)
        val requestCall = RequestHelper.createRequest(RequestCall.IAppData::class.java, null)
        call = requestCall.appData(params.urlParams)
        call!!.enqueue(this)
    }

    override fun onRequestStart() {
        mView.onRequestStart()
    }

    override fun onRequestStop() {
        mView.onRequestStop()
    }

    override fun onSuccess(response: Response<ResponseBase<AppDataResponse>>) {
        val model = response.body()
        if (model != null) {
            if (model.isHasError) {
                mView.onRequestError(model.message)
            } else {
                mView.onRequestSuccess(model.response)
            }
        } else {
            mView.onCriticalError()
        }
    }

    override fun onUnauthenticated(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onClientError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onServerError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onNetworkError(e: IOException) {
        mView.onRequestError(e.message)
    }

    override fun onUnexpectedError(t: Throwable) {
        mView.onRequestError(t.message)
    }


}
