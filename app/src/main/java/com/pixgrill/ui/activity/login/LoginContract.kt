package com.pixgrill.ui.activity.login

import com.pixgrill.base.BaseRequestPresenter
import com.pixgrill.base.BaseRequestView
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.model.response.UserResponse


interface LoginContract {

    interface View : BaseRequestView<UserResponse, Presenter> {

        fun onEmailAddressError(@GlobalTypeIntDef.GlobalType type: Int)

        fun onPasswordError(@GlobalTypeIntDef.GlobalType type: Int)
    }

    interface Presenter : BaseRequestPresenter {
        fun userLogin(email: String, password: String, type: String?)

        fun userLoginWithTwitterOrFb(accountId: String, type: String?)
    }
}
