package com.pixgrill.ui.activity.profile

import android.os.Bundle
import android.view.View
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.ui.fragment.userprofile.editprofile.EditProfileFragment
import com.pixgrill.ui.fragment.userprofile.ownprofile.OwnProfileFragment
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.include_toolbar_layout.*

class ProfileActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        toolbarTitle.text="Update Profile"

        ActivityUtils.replaceFragmentToActivity(supportFragmentManager,
                EditProfileFragment.newInstance(), R.id.fragmentContainer)
    }

    override fun onViewCreated() {
        super.onViewCreated()
        toolbarBack.setOnClickListener {
            finish()
        }
    }

    override fun onClickListener(view: View?) {
    }
}
