package com.pixgrill.ui.activity.home

import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SlidingPaneLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.pixgrill.R
import com.pixgrill.adapter.HomeDrawerAdapter
import com.pixgrill.alert.SweetAlertDialog
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.StaticData
import com.pixgrill.model.DrawerListModel
import com.pixgrill.ui.activity.addPost.AddPostActivity
import com.pixgrill.ui.activity.login.LoginActivity
import com.pixgrill.ui.activity.profile.ProfileActivity
import com.pixgrill.ui.activity.search.SearchActivity
import com.pixgrill.ui.fragment.events.listing.EventFragment
import com.pixgrill.ui.fragment.group.GroupListFragment
import com.pixgrill.ui.fragment.home.HomeFragment
import com.pixgrill.ui.fragment.notification.NotificationFragment
import com.pixgrill.ui.fragment.userprofile.ownprofile.OwnProfileFragment
import com.pixgrill.ui.fragment.wallet.WalletFragment
import com.pixgrill.util.ActivityUtils
import com.pixgrill.util.DisplayUtils
import com.pixgrill.util.Toast
import com.pixgrill.widget.PagerEnabledSlidingPaneLayout
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.include_toolbar_layout.toolbarEdit


class HomeActivity : BaseActivity(), AdapterView.OnItemClickListener {


    private lateinit var _slidingLayout: PagerEnabledSlidingPaneLayout
    private lateinit var _sliderListItems: ArrayList<DrawerListModel>
    private lateinit var _adapter: HomeDrawerAdapter
    private var _backPressedTime: Long = 0
    private val TIME_INTERVAL: Long = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        ActivityUtils.replaceFragmentToActivity(supportFragmentManager, HomeFragment.newInstance(), R.id.fragmentContainer)


    }

    override fun onViewCreated() {
        super.onViewCreated()
        _slidingLayout = slidingLayout
        _sliderListItems = StaticData.getDrawerList()
        _slidingLayout.setShadowResource(R.drawable.pane_layout_shadow)
        _adapter = HomeDrawerAdapter(applicationContext, _sliderListItems)
        val view = (this.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.content_menu_header, null, false)
        listDrawer.addHeaderView(view, null, false)
        view.setOnClickListener(this)
        listDrawer.adapter = _adapter

        toolbarMenu.setOnClickListener {
            onMenuClick()
        }
        listDrawer.onItemClickListener = this
        toolbarEdit.setOnClickListener {
            ActivityUtils.startActivity(this, ProfileActivity::class.java, null, false)
        }

        toolbarPostFeed.setOnClickListener {
            ActivityUtils.startActivity(this, AddPostActivity::class.java, null, false)
        }
        toolbarPostSearch.setOnClickListener {
            ActivityUtils.startActivity(this, SearchActivity::class.java, null, false)
        }
    }

    override fun onCreateViews(displayUtils: DisplayUtils) {
        super.onCreateViews(displayUtils)
        val leftSlidingLayoutParams = SlidingPaneLayout.LayoutParams(displayUtils.getWidthPercentage(80),
                ViewGroup.LayoutParams.MATCH_PARENT)
        leftSliding.layoutParams = leftSlidingLayoutParams
        _slidingLayout.parallaxDistance = displayUtils.getWidthPercentage(80) / 3
        _slidingLayout.sliderFadeColor = resources.getColor(android.R.color.transparent)
    }
    override fun onClickListener(view: View?) {
        if (_slidingLayout.isOpen) {
            _slidingLayout.closePane()
        }
        toolbarTitle.text = getString(R.string.title_my_profile);
        toolbarEdit.visibility = View.VISIBLE
        toolbarPostFeed.visibility = View.INVISIBLE
        toolbarPostSearch.visibility = View.GONE
        toolbar.setBackgroundResource(R.drawable.rect_round_gradiant_action_bar)
        ActivityUtils.replaceFragmentToActivity(supportFragmentManager, OwnProfileFragment.newInstance(), R.id.fragmentContainer)
    }
    override fun onItemClick(adapterView: AdapterView<*>?, view: View?, position: Int, l: Long) {
        if (_slidingLayout.isOpen) {
            _slidingLayout.closePane()
        }

        when (position) {
            1 -> {
                toolbarTitle.text = getString(R.string.title_home)
                toolbarPostFeed.visibility=View.VISIBLE
                toolbarPostSearch.visibility=View.VISIBLE
                toolbarEdit.visibility = View.GONE
                toolbar.setBackgroundResource(R.drawable.rect_gradiant_action_bar)
                ActivityUtils.replaceFragmentToActivity(supportFragmentManager, HomeFragment.newInstance(), R.id.fragmentContainer)
            }
            2 -> {
                toolbarTitle.text = getString(R.string.title_events)
                toolbarPostFeed.visibility=View.INVISIBLE
                toolbarPostSearch.visibility=View.INVISIBLE
                toolbarEdit.visibility = View.GONE
                toolbar.setBackgroundResource(R.drawable.rect_gradiant_action_bar)
                ActivityUtils.replaceFragmentToActivity(supportFragmentManager, EventFragment.newInstance(), R.id.fragmentContainer)
            }
            3 -> {
                toolbarTitle.text = getString(R.string.title_groups)
                toolbarPostFeed.visibility=View.INVISIBLE
                toolbarPostSearch.visibility=View.INVISIBLE
                toolbarEdit.visibility = View.GONE
                toolbar.setBackgroundResource(R.drawable.rect_gradiant_action_bar)
                ActivityUtils.replaceFragmentToActivity(supportFragmentManager, GroupListFragment.newInstance(), R.id.fragmentContainer)
            }
            4 -> {
                toolbarTitle.text = getString(R.string.title_wallet)
                toolbarPostFeed.visibility=View.INVISIBLE
                toolbarPostSearch.visibility=View.INVISIBLE
                toolbarEdit.visibility = View.GONE
                toolbar.setBackgroundResource(R.drawable.rect_gradiant_action_bar)
                ActivityUtils.replaceFragmentToActivity(supportFragmentManager, WalletFragment.newInstance(), R.id.fragmentContainer)
            }
            5 -> {
                toolbarTitle.text = getString(R.string.title_notification)
                toolbarPostFeed.visibility=View.INVISIBLE
                toolbarPostSearch.visibility=View.INVISIBLE
                toolbarEdit.visibility = View.GONE
                toolbar.setBackgroundResource(R.drawable.rect_gradiant_action_bar)
                ActivityUtils.replaceFragmentToActivity(supportFragmentManager, NotificationFragment.newInstance(), R.id.fragmentContainer)

            }

            11 -> {
                SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure?")
                        .setContentText("You want to logout?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener {
                            _preferenceData.userDetails = null
                            ActivityUtils.startActivity(this, LoginActivity::class.java, null, true) }
                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                        .show()

            }
        }

    }

    fun onMenuClick() {
        if (_slidingLayout.isOpen) {
            _slidingLayout.closePane()
        } else {
            _slidingLayout.openPane()
        }
    }

    override fun onBackPressed() {
        if (_slidingLayout.isOpen) {
            _slidingLayout.closePane()
        }

        if (supportFragmentManager.backStackEntryCount > 0) run { supportFragmentManager.popBackStackImmediate() }
        else {
            if (_backPressedTime + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed()
                return
            } else {
                this.Toast(getString(R.string.text_exit_message))
            }
            _backPressedTime = System.currentTimeMillis()
        }
    }
}
