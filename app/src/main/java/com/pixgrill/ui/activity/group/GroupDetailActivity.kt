package com.pixgrill.ui.activity.group

import android.app.Activity
import android.graphics.Point
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import com.pixgrill.R
import com.pixgrill.R.id.leaveItem
import com.pixgrill.R.id.reportItem
import com.pixgrill.adapter.FeedsAdapter
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.BundleArguments
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.FeedResponse
import com.pixgrill.ui.activity.addPost.AddPostActivity
import com.pixgrill.ui.activity.postdetail.PostDetailActivity
import com.pixgrill.util.ActivityUtils
import com.pixgrill.util.Toast
import kotlinx.android.synthetic.main.content_group_detail.*
import kotlinx.android.synthetic.main.include_common_toolbar_layout.*

class GroupDetailActivity : BaseActivity(), OnRecyclerViewClickListener, PopupMenu.OnMenuItemClickListener {

    lateinit var feedsAdapter: FeedsAdapter
    val _feedList = ArrayList<FeedResponse>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_detail)
        toolbarTitle.text = getString(R.string.title_groups)
        toolbarPostFeed.visibility = View.VISIBLE
    }

    override fun onViewCreated() {
        super.onViewCreated()
        toolbarBack.setOnClickListener { finish() }
        toolbarPostFeed.setOnClickListener {
            ActivityUtils.startActivity(this, AddPostActivity::class.java, null, false)
        }

        var feeds = FeedResponse()
        feeds.userImage = ""
        feeds.userName = ""
        for (i in 1..10) {
            _feedList.add(feeds)
        }

        feedsAdapter = FeedsAdapter(_feedList, this, this)
        postListRecyclerView.layoutManager = LinearLayoutManager(this!!) as RecyclerView.LayoutManager
        postListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        this as Activity,
                        0,
                        0
                )
        )
        postListRecyclerView.adapter = feedsAdapter
        postListRecyclerView.isNestedScrollingEnabled = false

        buttonActionMore.setOnClickListener {
            val popup = PopupMenu(this, it)
            popup.setOnMenuItemClickListener(this)
            popup.inflate(R.menu.popup_layout)
            popup.show()
        }
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
        val feed = clickedObject as FeedResponse
        var imageList = mutableListOf<String>(feed.userImage!!)
        for (i in 1..3){
            imageList.add("")
        }
        var bundle = Bundle()
        bundle.putStringArrayList(BundleArguments.ARGS_POST_IMAGE_LIST, imageList as java.util.ArrayList<String>?)
        ActivityUtils.startActivity(this, PostDetailActivity::class.java, bundle, false)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when(item?.itemId){
            reportItem -> {
                return true
            }
            leaveItem -> {
                return true
            }
        }
        return false
    }

    override fun onClickListener(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
