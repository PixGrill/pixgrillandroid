package com.pixgrill.ui.activity.comment

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pixgrill.R
import com.pixgrill.adapter.CommentsAdapter
import com.pixgrill.base.BaseActivity
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.CommentModel
import kotlinx.android.synthetic.main.content_comment.*

class CommentActivity : BaseActivity(), OnRecyclerViewClickListener {

    lateinit var commentAdapter: CommentsAdapter
    val _commentList = ArrayList<CommentModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment)
    }

    override fun onViewCreated() {
        super.onViewCreated()
        var comment = CommentModel()
        comment.commentId = 1
        comment.nickName = "Midhun"
        comment.comment = "Its my new comment.."

        var comment2 = CommentModel()
        comment2.commentId = 2
        comment2.nickName = "Athul Krishna"
        comment2.comment = "haii, sbjfh shfdv SSHS djhsfhj hdgfsdfsd hdfsd..."
        for (i in 1..5) {
            _commentList.add(comment)
            _commentList.add(comment2)
        }

        commentAdapter = CommentsAdapter(_commentList, this)
        commentListRecyclerView.layoutManager = LinearLayoutManager(this!!) as RecyclerView.LayoutManager
        commentListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        this as Activity,
                        0,
                        0
                )
        )
        commentListRecyclerView.adapter = commentAdapter

    }

    override fun onClickListener(view: View?) {
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
    }

}
