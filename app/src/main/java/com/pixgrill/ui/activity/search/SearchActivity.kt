package com.pixgrill.ui.activity.search

import android.os.Bundle
import android.view.View
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.ui.fragment.search.SearchFragment
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.include_common_toolbar_layout.*

class SearchActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        ActivityUtils.replaceFragmentToActivity(supportFragmentManager, SearchFragment.newInstance(), R.id.fragmentContainer)
    }

    override fun onViewCreated() {
        super.onViewCreated()
        toolbarTitle.setText(getString(R.string.search_post_friends))
        toolbarBack.setOnClickListener {
            finish()
        }
    }
    override fun onClickListener(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
