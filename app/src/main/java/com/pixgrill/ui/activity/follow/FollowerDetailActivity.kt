package com.pixgrill.ui.activity.follow

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pixgrill.R
import com.pixgrill.adapter.FeedsAdapter
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.BundleArguments
import com.pixgrill.data.BundleArguments.ARGS_FOLLOWER_OBJECT
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.FollowerModel
import com.pixgrill.model.response.FeedResponse
import com.pixgrill.ui.activity.postdetail.PostDetailActivity
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.feed_layout.*
import kotlinx.android.synthetic.main.include_toolbar_layout.*

class FollowerDetailActivity : BaseActivity() , OnRecyclerViewClickListener {

    lateinit var feedsAdapter: FeedsAdapter
    val _feedList = ArrayList<FeedResponse>()
    lateinit var followerObject: FollowerModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_follower_detail)
        toolbarEdit.visibility = View.INVISIBLE
        toolbarTitle.text = getString(R.string.title_profile)
       /* val bundle = intent.extras
        if (bundle != null) {
            followerObject = bundle.getParcelable(ARGS_FOLLOWER_OBJECT)
        }*/

        toolbarBack.setOnClickListener {
            finish()
        }
    }

    override fun onViewCreated() {
        super.onViewCreated()

        var feeds = FeedResponse()
        feeds.userImage = ""
        feeds.userName = ""
        for (i in 1..10) {
            _feedList.add(feeds)
        }



        feedsAdapter = FeedsAdapter(_feedList, this, this)
        feedListRecyclerView.layoutManager = LinearLayoutManager(this!!) as RecyclerView.LayoutManager
        feedListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        this as Activity,
                        0,
                        0
                )
        )
        feedListRecyclerView.adapter = feedsAdapter
        feedListRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
        val feed = clickedObject as FeedResponse
        var imageList = mutableListOf<String>(feed.userImage!!)
        for (i in 1..3){
            imageList.add("")
        }
        var bundle = Bundle()
        bundle.putStringArrayList(BundleArguments.ARGS_POST_IMAGE_LIST, imageList as java.util.ArrayList<String>?)
        ActivityUtils.startActivity(this, PostDetailActivity::class.java, bundle, false)
    }
    override fun onClickListener(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
