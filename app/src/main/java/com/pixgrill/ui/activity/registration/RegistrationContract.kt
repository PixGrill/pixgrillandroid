package com.pixgrill.ui.activity.registration

import com.pixgrill.base.BaseRequestPresenter
import com.pixgrill.base.BaseRequestView
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.model.response.UserResponse


interface RegistrationContract {

    interface View : BaseRequestView<UserResponse, Presenter> {
        fun onNickNameError(@GlobalTypeIntDef.GlobalType type: Int)

        fun onEmailAddressError(@GlobalTypeIntDef.GlobalType type: Int)

        fun onPasswordError(@GlobalTypeIntDef.GlobalType type: Int)

    }

    interface Presenter : BaseRequestPresenter {
        fun userSignUp(nickName: String, email: String, password: String)
    }
}
