package com.pixgrill.ui.activity.postdetail

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pixgrill.R
import com.pixgrill.adapter.PostDetailAdapter
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.BundleArguments.ARGS_POST_IMAGE_LIST
import com.pixgrill.decorator.ItemOffsetDecoration
import kotlinx.android.synthetic.main.content_post_detail.*
import kotlinx.android.synthetic.main.include_toolbar_layout.*

class PostDetailActivity : BaseActivity() {

    lateinit var postDetailAdapter: PostDetailAdapter
    private var _imageList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)
        toolbarTitle.text = getString(R.string.title_post_detail);
        val bundle = intent.extras
        if (bundle != null) {
            _imageList = bundle.getStringArrayList(ARGS_POST_IMAGE_LIST)
            postDetailAdapter = PostDetailAdapter(_imageList, this)
            postDetailRecyclerView.adapter = postDetailAdapter
        }
    }

    override fun onViewCreated() {
        super.onViewCreated()
        toolbarBack.setOnClickListener { finish() }

        postDetailRecyclerView.layoutManager = LinearLayoutManager(this!!) as RecyclerView.LayoutManager
        postDetailRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        this as Activity,
                        0,
                        0
                )
        )
        postDetailRecyclerView.isNestedScrollingEnabled = false

    }

    override fun onClickListener(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
