package com.pixgrill.ui.activity.addPost

import com.pixgrill.base.BaseRequestPresenter
import com.pixgrill.base.BaseRequestView
import com.pixgrill.model.response.UserResponse

interface AddPostContract {
    interface View : BaseRequestView<UserResponse, Presenter> {

    }

    interface Presenter : BaseRequestPresenter {
        fun addPost()
    }
}