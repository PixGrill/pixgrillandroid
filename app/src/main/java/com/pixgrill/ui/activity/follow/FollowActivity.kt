package com.pixgrill.ui.activity.follow

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pixgrill.R
import com.pixgrill.adapter.FollowAdapter
import com.pixgrill.base.BaseActivity
import com.pixgrill.data.BundleArguments.ARGS_FOLLOWER_OBJECT
import com.pixgrill.data.BundleArguments.ARGS_IS_FOLLOW
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.FollowerModel
import com.pixgrill.model.response.UserResponse
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.content_follow.*
import kotlinx.android.synthetic.main.include_common_toolbar_layout.*

class FollowActivity :BaseActivity(), OnRecyclerViewClickListener {

    lateinit var folllowAdapter: FollowAdapter
    val _followList = ArrayList<FollowerModel>()
    var isFolllow: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_follow)

        val bundle = intent.extras
        if (bundle != null) {
            isFolllow = bundle.getBoolean(ARGS_IS_FOLLOW)
        }
        if(isFolllow){
            toolbarTitle.text = getString(R.string.title_followers)
        }else{
            toolbarTitle.text = getString(R.string.title_followings)
        }

        toolbarBack.setOnClickListener {
            finish()
        }
    }

    override fun onViewCreated() {
        super.onViewCreated()

        var user = UserResponse("","Male", "Midhun M S", "987564", "", 85, "", "","", "Wildlife Photography", "", "", "")
        var followerModel = FollowerModel()
        followerModel.user = user
        followerModel.isFollowerModel = true

        user = UserResponse("","Male", "John Dioe", "987564", "", 86, "", "","", "Wildlife Photography", "", "", "")
        var followerModel1 = FollowerModel()
        followerModel1.user = user
        followerModel1.isFollowerModel = false

        for (i in 1..10) {
            _followList.add(followerModel)
            _followList.add(followerModel1)
        }

        folllowAdapter = FollowAdapter(_followList, this,this)
        followListRecyclerView.layoutManager = LinearLayoutManager(this!!) as RecyclerView.LayoutManager
        followListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        this as Activity,
                        0,
                        0
                )
        )
        followListRecyclerView.adapter = folllowAdapter
        followListRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
       /* val user = clickedObject as FollowerModel
        val bundle = Bundle()
        bundle.putParcelable(ARGS_FOLLOWER_OBJECT, user)*/
        ActivityUtils.startActivity(this, FollowerDetailActivity::class.java, null, false)
    }

    override fun onClickListener(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
