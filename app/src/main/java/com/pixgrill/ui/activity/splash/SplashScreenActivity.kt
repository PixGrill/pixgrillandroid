package com.pixgrill.ui.activity.splash

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.google.android.gms.ads.MobileAds
import com.pixgrill.PixgrillApplication
import com.pixgrill.R
import com.pixgrill.base.BaseActivity
import com.pixgrill.model.response.AppDataResponse
import com.pixgrill.seven.ui.activity.splash.SplashScreenPresenter
import com.pixgrill.ui.activity.home.HomeActivity
import com.pixgrill.ui.activity.login.LoginActivity
import com.pixgrill.util.ActivityUtils
import com.pixgrill.util.DisplayUtils


class SplashScreenActivity : BaseActivity(), SplashScreenContract.View {


    private var mPresenter: SplashScreenContract.Presenter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        SplashScreenPresenter(this)
        MobileAds.initialize(this, getString(R.string.admob_app_id))
    }

    override fun onCreateViews(displayUtils: DisplayUtils) {
        super.onCreateViews(displayUtils)
    }
    override fun onClickListener(view: View?) {

    }
    override fun onViewCreated() {
        super.onViewCreated()
        if (_preferenceData.userDetails != null) {
            navigateToHomePage()
        } else {
            navigateToLoginPage()
        }
    }

    override fun onRequestStart() {
    }

    override fun onRequestStop() {
    }

    override fun onRequestError(message: String?) {
    }

    override fun onCriticalError() {
    }

    override fun onRequestSuccess(response: AppDataResponse?) {
    }

    override fun setPresenter(presenter: SplashScreenContract.Presenter?) {
    }

    override fun navigateToLoginPage() {
        Handler().postDelayed({ ActivityUtils.startActivity(this, LoginActivity::class.java, null, true) }, 2000)
    }

    override fun navigateToHomePage() {
        Handler().postDelayed({ ActivityUtils.startActivity(this, HomeActivity::class.java, null, true) }, 2000)
    }

}
