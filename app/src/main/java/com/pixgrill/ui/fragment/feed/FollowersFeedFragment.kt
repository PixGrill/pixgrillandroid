package com.pixgrill.ui.fragment.feed


import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pixgrill.R
import com.pixgrill.adapter.FeedsAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.data.BundleArguments
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.FeedResponse
import kotlinx.android.synthetic.main.feed_layout.*
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.ui.activity.addPost.AddPostActivity
import com.pixgrill.ui.activity.postdetail.PostDetailActivity
import com.pixgrill.ui.fragment.userprofile.ownprofile.OwnProfileFragment
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.feed_layout.view.*


class FollowersFeedFragment : BaseFragment(), OnRecyclerViewClickListener {


    lateinit var feedsAdapter: FeedsAdapter
    val _feedList = ArrayList<FeedResponse>()


    companion object {
        fun newInstance(): FollowersFeedFragment {
            val fragment = FollowersFeedFragment()
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.feed_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var feeds = FeedResponse()
        feeds.userImage = ""
        feeds.userName = ""
        for (i in 1..10) {
            _feedList.add(feeds)
        }



        feedsAdapter = FeedsAdapter(_feedList, this, this!!.activity!!)
        feedListRecyclerView.layoutManager = LinearLayoutManager(activity!!) as RecyclerView.LayoutManager
        feedListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        activity as Activity,
                        0,
                        0
                )
        )
        feedListRecyclerView.adapter = feedsAdapter
        feedListRecyclerView.isNestedScrollingEnabled = false
        postImageLayout.setOnClickListener{
            ActivityUtils.startActivity(activity!!, AddPostActivity::class.java, null, false)
        }
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
        val feed = clickedObject as FeedResponse
        var imageList = mutableListOf<String>(feed.userImage!!)
        for (i in 1..3){
            imageList.add("")
        }
        var bundle = Bundle()
        bundle.putStringArrayList(BundleArguments.ARGS_POST_IMAGE_LIST, imageList as java.util.ArrayList<String>?)
        ActivityUtils.startActivity(this!!.activity!!, PostDetailActivity::class.java, bundle, false)
    }
}
