package com.pixgrill.ui.fragment.userprofile.editprofile

import com.pixgrill.base.BaseRequestPresenter
import com.pixgrill.base.BaseRequestView
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.model.response.UserResponse

interface EditProfileContract {

    interface View : BaseRequestView<UserResponse, EditProfileContract.Presenter> {
        fun onEmtyError(@GlobalTypeIntDef.GlobalType type: Int, field: String)
    }

    interface Presenter : BaseRequestPresenter {
        fun userEditProfile(imageUri: String?, facebookUsername: String, categoryId: Int, cameraSpec: String, dob: String, gender: String, twitterUsername: String, mobile: String, bio: String)
    }
}