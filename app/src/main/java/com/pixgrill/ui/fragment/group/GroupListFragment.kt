package com.pixgrill.ui.fragment.group


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pixgrill.R
import com.pixgrill.adapter.GroupsAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.EventResponse
import com.pixgrill.ui.activity.group.GroupDetailActivity
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.fragment_group_list.*


class GroupListFragment : BaseFragment(), OnRecyclerViewClickListener {


    lateinit var feedsAdapter: GroupsAdapter
    val _eventsList = ArrayList<EventResponse>()

    companion object {
        fun newInstance(): GroupListFragment {
            val fragment = GroupListFragment()
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var events = EventResponse()
        events.eventStatus = 1
        _eventsList.add(events)

        var events1 = EventResponse()
        events1.eventStatus = 0
        _eventsList.add(events1)

        var events2 = EventResponse()
        events2.eventStatus = 2
        _eventsList.add(events2)
        var events3 = EventResponse()
        events3.eventStatus = 2
        _eventsList.add(events3)
        var events4 = EventResponse()
        events4.eventStatus = 2
        _eventsList.add(events4)
        var events5 = EventResponse()
        events5.eventStatus = 2
        _eventsList.add(events5)
        var events6 = EventResponse()
        events6.eventStatus = 2
        _eventsList.add(events6)
        var events7 = EventResponse()
        events7.eventStatus = 2
        _eventsList.add(events7)

        feedsAdapter = GroupsAdapter(_eventsList, this, this!!.activity!!)
        groupsRecyclerView.layoutManager = LinearLayoutManager(activity!!) as RecyclerView.LayoutManager
        groupsRecyclerView.adapter = feedsAdapter
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
        ActivityUtils.startActivity(this!!.activity!!, GroupDetailActivity::class.java, null, false)
    }
}
