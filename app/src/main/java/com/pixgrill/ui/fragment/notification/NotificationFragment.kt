package com.pixgrill.ui.fragment.notification

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pixgrill.R
import com.pixgrill.adapter.NotificationAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.NotificationModel
import com.pixgrill.util.RecyclerItemTouchHelper
import kotlinx.android.synthetic.main.fragment_notification.*
import android.support.v7.widget.helper.ItemTouchHelper



class NotificationFragment : BaseFragment(), OnRecyclerViewClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {


    lateinit var notificationAdapter: NotificationAdapter
    val _notificationList = ArrayList<NotificationModel>()

    companion object {
        fun newInstance(): NotificationFragment {
            val fragment = NotificationFragment()
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var notification = NotificationModel()
        notification.nickName = "Midhun M S"
        notification.content = "Martha Rojas comment on your recent post"
        notification.time = "2 hours ago"
        for (i in 1..10) {
            _notificationList.add(notification)
        }

        notificationAdapter = NotificationAdapter(_notificationList, this, this!!.activity!!)
        notificationListRecyclerView.layoutManager = LinearLayoutManager(activity!!) as RecyclerView.LayoutManager
        notificationListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        activity as Activity,
                        0,
                        0
                )
        )
        notificationListRecyclerView.adapter = notificationAdapter
        notificationListRecyclerView.isNestedScrollingEnabled = false
        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(notificationListRecyclerView)

    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
    }
}