package com.pixgrill.ui.fragment.search

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pixgrill.R
import com.pixgrill.adapter.SearchUserAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.decorator.ItemOffsetDecoration
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.UserResponse
import com.pixgrill.ui.activity.follow.FollowerDetailActivity
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.fragment_search_user.*

class SearchUserFragment : BaseFragment(), OnRecyclerViewClickListener {

    lateinit var userAdapter: SearchUserAdapter
    val _userList = ArrayList<UserResponse>()

    companion object {
        fun newInstance(): SearchUserFragment {
            val fragment = SearchUserFragment()
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var user = UserResponse("","Male", "Midhun M S", "987564", "", 85, "", "","", "Wildlife Photography", "", "", "")
        var user1 = UserResponse("","Male", "John Dioe", "987564", "", 86, "", "","", "Wildlife Photography", "", "", "")

        for (i in 1..10) {
            _userList.add(user)
            _userList.add(user1)
        }
        userAdapter = SearchUserAdapter(_userList, this, this!!.activity!!)
        userListRecyclerView.layoutManager = LinearLayoutManager(activity!!) as RecyclerView.LayoutManager
        userListRecyclerView.addItemDecoration(
                ItemOffsetDecoration(
                        activity as Activity,
                        0,
                        0
                )
        )
        userListRecyclerView.adapter = userAdapter
        userListRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
        ActivityUtils.startActivity(this!!.activity!!, FollowerDetailActivity::class.java, null, false)
    }

}
