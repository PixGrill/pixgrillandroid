package com.pixgrill.ui.fragment.userprofile.ownprofile


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pixgrill.R
import com.pixgrill.base.BaseFragment
import com.pixgrill.data.BundleArguments.ARGS_IS_FOLLOW
import com.pixgrill.ui.activity.follow.FollowActivity
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.fragment_own_profile.*
import kotlinx.android.synthetic.main.include_toolbar_layout.*


/**
 * A simple [Fragment] subclass.
 *
 */
class OwnProfileFragment : BaseFragment() {

    companion object {

        fun newInstance(): OwnProfileFragment {
            return OwnProfileFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_own_profile, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        activity.toolbarEdit.visibility = View.VISIBLE

        val user = _preferenceData.userDetails
        if(user != null){
            textViewNickName.text = user.nickName
            textViewCameraSpec.text = user.cameraSpec
            textViewBio.text = user.bio
            textViewFacebookId.text = user.facebookUserName
            textViewTwitterId.text = user.twitterUserName
            imageProfile.setImageURI(user.profileImageUrl)
        }

        layoutFollowings.setOnClickListener {
            var bundle = Bundle()
            bundle.putBoolean(ARGS_IS_FOLLOW, false)
            ActivityUtils.startActivity(this!!.activity!!, FollowActivity::class.java, bundle, false)
        }

        layoutFollowers.setOnClickListener {
            var bundle = Bundle()
            bundle.putBoolean(ARGS_IS_FOLLOW, true)
            ActivityUtils.startActivity(this!!.activity!!, FollowActivity::class.java, bundle, false)
        }
    }
}
