package com.pixgrill.ui.fragment.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pixgrill.R
import com.pixgrill.adapter.PageAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.ui.activity.addPost.AddPostActivity
import com.pixgrill.ui.activity.event.EventDetailsActivity
import com.pixgrill.ui.fragment.feed.FollowersFeedFragment
import com.pixgrill.ui.fragment.feed.PopularFeedFragment
import com.pixgrill.ui.fragment.feed.RecentFeedFragment
import com.pixgrill.util.ActivityUtils
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : BaseFragment() {

    lateinit var pageAdapter: PageAdapter

    companion object {
        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pageAdapter = PageAdapter(childFragmentManager)
        pageAdapter.add(PopularFeedFragment.newInstance(), "Popular")
        pageAdapter.add(RecentFeedFragment.newInstance(), "Recent")
        pageAdapter.add(FollowersFeedFragment.newInstance(), "Followers Post")
        viewPager?.adapter = pageAdapter
        tabLayout?.setupWithViewPager(viewPager)

    }

}
