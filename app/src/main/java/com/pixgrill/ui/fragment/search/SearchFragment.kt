package com.pixgrill.ui.fragment.search

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pixgrill.R
import com.pixgrill.adapter.SearchPageAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.ui.fragment.feed.FollowersFeedFragment
import com.pixgrill.ui.fragment.feed.PopularFeedFragment
import com.pixgrill.ui.fragment.feed.RecentFeedFragment
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment() {


    lateinit var pageAdapter: SearchPageAdapter

    companion object {
        fun newInstance(): SearchFragment {
            val fragment = SearchFragment()
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pageAdapter = SearchPageAdapter(childFragmentManager)
        pageAdapter.add(SearchFeedFragment.newInstance(), "Post")
        pageAdapter.add(SearchUserFragment.newInstance(), "Users")
        viewPager?.adapter = pageAdapter
        tabLayout?.setupWithViewPager(viewPager)
    }

}
