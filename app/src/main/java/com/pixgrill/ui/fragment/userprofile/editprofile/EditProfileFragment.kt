package com.pixgrill.ui.fragment.userprofile.editprofile


import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.pixgrill.R
import com.pixgrill.base.BaseFragment
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.data.StaticData
import com.pixgrill.dialog.SheetFragment
import com.pixgrill.model.SheetListModel
import com.pixgrill.model.response.UserResponse
import com.pixgrill.ui.activity.home.HomeActivity
import com.pixgrill.util.ActivityUtils
import com.pixgrill.util.Preconditions
import com.pixgrill.util.showSnackBarError
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.include_toolbar_layout.*
import android.provider.MediaStore
import android.content.Intent
import android.app.Activity.RESULT_OK
import android.content.res.Resources
import android.net.Uri
import android.os.AsyncTask
import android.webkit.MimeTypeMap
import com.amazonaws.mobile.client.AWSMobileClient
import java.io.File
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.regions.Regions
import com.pixgrill.PixgrillApplication
import java.lang.Exception


/**
 * A simple [Fragment] subclass.
 *
 */
class EditProfileFragment : BaseFragment(), EditProfileContract.View, SheetFragment.Listener {

    private var mPresenter: EditProfileContract.Presenter? = null
    private val SHEET_DIALOG_ID_CATEGORY = 1000
    private val SHEET_DIALOG_ID_GENDER = 1001
    private val SHEET_DIALOG_ID_PHOTO = 1002
    private var categoryId: Int = -1
    private var genderId: Int = -1
    companion object {

        fun newInstance(): EditProfileFragment {
            return EditProfileFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EditProfilePresenter(this)
        mPresenter?.start(activity)

       // AWSMobileClient.getInstance().initialize(context).execute()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.toolbarEdit?.visibility = View.INVISIBLE
        activity?.toolbarTitle?.text = "Edit Profile"

        editCategory.setOnClickListener {

            val sheetFragment = SheetFragment.newInstance(SHEET_DIALOG_ID_CATEGORY, StaticData.getCategoryList(), "CATEGORY")
            sheetFragment.show(activity?.supportFragmentManager, "Category")
        }
        editGender.setOnClickListener {

            val sheetFragment = SheetFragment.newInstance(SHEET_DIALOG_ID_GENDER, StaticData.getGenderPickerList(), "GENDER")
            sheetFragment.show(activity?.supportFragmentManager, "Gender")
        }
        changePicture.setOnClickListener {

            val sheetFragment = SheetFragment.newInstance(SHEET_DIALOG_ID_PHOTO, StaticData.getCameraPickerList(), "CHOOSE")
            sheetFragment.show(activity?.supportFragmentManager, "Photo")
        }
        val user = _preferenceData.userDetails
        if (user != null) {
            imageProfile.setImageURI(user.profileImageUrl)
            editFacebookUsername.setText(user.facebookUserName)
            editCategory.setText(user.category)
            editCamera.setText(user.cameraSpec)
            editDOB.setText(user.dob)
            editGender.setText(user.gender)
            editTwitterUsername.setText(user.twitterUserName)
            editMobile.setText(user.mobileNumber)
            editBio.setText(user.bio)
            StaticData.getGenderPickerList().forEach {
                if (user.gender == it.name) {
                    genderId = it.id
                }
            }
        }

        saveProfile.setOnClickListener {
            mPresenter?.userEditProfile("imageUri", editFacebookUsername.text.toString(), categoryId, editCamera.text.toString(),
                    editDOB.text.toString(), genderId.toString(), editTwitterUsername.text.toString(), editMobile.text.toString(), editBio.text.toString())
        }
    }

    override fun itemStateSelected(sheetModel: Any?, dialogId: Int) {
        val model = (sheetModel as SheetListModel)
        if (dialogId == SHEET_DIALOG_ID_CATEGORY) {
            editCategory.setText(model.name)
            categoryId = model.id
        } else if (dialogId == SHEET_DIALOG_ID_GENDER) {
            editGender.setText(model.name)
            genderId = model.id
        } else {
            if (model.id == 1) {
                val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(pickPhoto, 0)
            } else {
                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, 1)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode === RESULT_OK) {
            var uri = data?.data
            val path = getPathFromURI(uri!!)
            if (path != null) {
                val f = File(path)
                uri = Uri.fromFile(f)

               // UploadImage(uri, path).execute("", "", "");
            }
        }


    }

    fun getPathFromURI(contentUri: Uri): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activity?.contentResolver?.query(contentUri, proj, null, null, null)
        if (cursor!!.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    override fun onRequestStart() {
        progressView.visibility = View.VISIBLE
    }

    override fun onRequestStop() {
        progressView.visibility = View.INVISIBLE
    }

    override fun onRequestError(message: String?) {
        progressView.visibility = View.VISIBLE
    }

    override fun onCriticalError() {
    }

    override fun onRequestSuccess(response: UserResponse?) {
        if (response != null) {
            _preferenceData.userDetails = response
            ActivityUtils.startActivity(this!!.activity!!, HomeActivity::class.java, null, true)
        }
    }

    override fun setPresenter(presenter: EditProfileContract.Presenter?) {
        mPresenter = Preconditions.checkNotNull(presenter)
    }

    override fun onEmtyError(type: Int, field: String) {
        if (type == GlobalTypeIntDef.EMPTY_ERROR) {
            showSnackBarError(
                    this!!.context!!,
                    String.format(getString(R.string.text_error_format), field + getString(R.string.text_field_empty_error)),
                    Snackbar.LENGTH_INDEFINITE)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.cancelRequest()
    }

    class UploadImage : AsyncTask<String, String, String> {
        var fileUri: Uri? = null
        var filePath: String? = null
        var credentials = BasicAWSCredentials(PixgrillApplication.getContext().getString(R.string.s3_key_id), PixgrillApplication.getContext().getString(R.string.s3_sectret_id))
        var s3Client = AmazonS3Client(credentials)

        constructor(fileUri : Uri, filePath: String){
            this.fileUri = fileUri
            this.filePath = filePath
        }
        override fun doInBackground(vararg params: String?): String? {
            val file = File(filePath)

            val transferUtility = TransferUtility.builder()
                    .context(PixgrillApplication.getContext())
                    .awsConfiguration(AWSMobileClient.getInstance().configuration)
                    .s3Client(s3Client)
                    .build()

            val uploadObserver = transferUtility.upload("jsaS3/" + file.name + "." + getFileExtension(fileUri), file)

            uploadObserver.setTransferListener(object : TransferListener {

                override fun onStateChanged(id: Int, state: TransferState) {
                    if (TransferState.COMPLETED == state) {
                        Toast.makeText(PixgrillApplication.getContext(), "Upload Completed!", Toast.LENGTH_SHORT).show()

                        file.delete()
                    } else if (TransferState.FAILED == state) {
                        file.delete()
                    }
                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                    val percentDone = percentDonef.toInt()

                    //tv_file_name.text = "ID:$id|bytesCurrent: $bytesCurrent|bytesTotal: $bytesTotal|$percentDone%"
                }

                override fun onError(id: Int, ex: Exception) {
                    ex.printStackTrace()
                }

            })
            return uploadObserver.state.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            Toast.makeText(PixgrillApplication.getContext(), result, Int.MIN_VALUE)
        }

        private fun getFileExtension(uri: Uri?): String {
            val contentResolver = PixgrillApplication.getContext().contentResolver
            val mime = MimeTypeMap.getSingleton()

            return mime.getExtensionFromMimeType(contentResolver.getType(uri))
        }
    }
}
