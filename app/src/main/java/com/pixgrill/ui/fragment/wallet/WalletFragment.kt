package com.pixgrill.ui.fragment.wallet


import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pixgrill.R
import com.pixgrill.adapter.WalletAdapter
import com.pixgrill.base.BaseFragment
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.WalletResponse
import kotlinx.android.synthetic.main.fragment_wallet.*
import android.support.v7.widget.DividerItemDecoration



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */


class WalletFragment : BaseFragment(), OnRecyclerViewClickListener {


    lateinit var walletAdapter: WalletAdapter
    val _walletList = ArrayList<WalletResponse>()

    companion object {

        fun newInstance(): WalletFragment {
            return WalletFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wallet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var walletList = WalletResponse()
        walletList.txnId=1
        _walletList.add(walletList)

        var walletList1 = WalletResponse()
        walletList.txnId=1
        _walletList.add(walletList1)

        var walletList2 = WalletResponse()
        walletList.txnId=1
        _walletList.add(walletList2)

        var walletList3 = WalletResponse()
        walletList.txnId=1
        _walletList.add(walletList3)



        walletAdapter=WalletAdapter(_walletList, this, this!!.activity!!)
        walletRecyclerView.layoutManager = LinearLayoutManager(activity!!) as RecyclerView.LayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        walletRecyclerView.addItemDecoration(itemDecor)
        walletRecyclerView.adapter = walletAdapter
    }
    override fun onRecyclerViewClick(position: Int, clickedObject: Any) {
    }
}
