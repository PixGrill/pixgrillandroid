package com.pixgrill.ui.fragment.userprofile.editprofile

import android.content.Context
import android.text.TextUtils
import com.pixgrill.base.ResponseBase
import com.pixgrill.data.GlobalTypeIntDef
import com.pixgrill.data.StaticData
import com.pixgrill.model.UserRequestModel
import com.pixgrill.model.response.UserResponse
import com.pixgrill.network.ErrorHandlingAdapter
import com.pixgrill.network.RequestCall
import com.pixgrill.network.RequestHelper
import com.pixgrill.util.Preconditions
import retrofit2.Response
import java.io.IOException

class EditProfilePresenter internal constructor(view: EditProfileContract.View) : EditProfileContract.Presenter, ErrorHandlingAdapter.RetroCallback<ResponseBase<UserResponse>> {

    lateinit var header: HashMap<String, String>
    var call: ErrorHandlingAdapter.RetroCall<ResponseBase<UserResponse>>? = null
    private val mView: EditProfileContract.View = Preconditions.checkNotNull<EditProfileContract.View>(view, view.javaClass.simpleName)

    init {
        mView.setPresenter(this)
    }

    override fun onRequestStart() {
        mView.onRequestStart()
    }

    override fun onRequestStop() {
        mView.onRequestStop()
    }

    override fun onSuccess(response: Response<ResponseBase<UserResponse>>) {
        val model = response.body()
        if (model != null) {
            if (model.isHasError) {
                mView.onRequestError(model.message)
            } else {
                mView.onRequestSuccess(model.response)
            }
        } else {
            mView.onCriticalError()
        }
    }

    override fun onUnauthenticated(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onClientError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onServerError(response: Response<*>) {
        mView.onRequestError(response.message())
    }

    override fun onNetworkError(e: IOException) {
        mView.onRequestError(e.message)
    }

    override fun onUnexpectedError(t: Throwable) {
        mView.onRequestError(t.message)
    }

    override fun start(context: Context?) {
        header = StaticData.getHeader(context)
    }

    override fun userEditProfile(imageUri: String?, facebookUserName: String, categoryId: Int, cameraSpec: String, dob: String, gender: String, twitterUserName: String, mobile: String, bio: String) {
        /*if (TextUtils.equals(categoryId.toString(), "-1")) {
            mView.onEmtyError(GlobalTypeIntDef.EMPTY_ERROR, "Category ")
        }
        else*/ if (TextUtils.isEmpty(cameraSpec)) {
            mView.onEmtyError(GlobalTypeIntDef.EMPTY_ERROR, "Camera Spec ")
        }
        else if (TextUtils.isEmpty(dob)) {
            mView.onEmtyError(GlobalTypeIntDef.EMPTY_ERROR, "DOB ")
        }
        else if (TextUtils.equals(gender, "-1")) {
            mView.onEmtyError(GlobalTypeIntDef.EMPTY_ERROR, "Gender ")
        }
        else if (TextUtils.isEmpty(mobile)) {
            mView.onEmtyError(GlobalTypeIntDef.EMPTY_ERROR, "Mobile ")
        }else if (TextUtils.isEmpty(bio)) {
            mView.onEmtyError(GlobalTypeIntDef.EMPTY_ERROR, "Bio ")
        } else {
            val userRequest = UserRequestModel()

            userRequest.categoryId = categoryId
            userRequest.cameraSpec = cameraSpec
            userRequest.dob = dob
            userRequest.mobileNumber = mobile
            userRequest.bio = bio
            userRequest.profileImageUrl = imageUri
            userRequest.gender = gender
            userRequest.facebookUserName = facebookUserName
            userRequest.twitterUserName = twitterUserName


            val requestCall = RequestHelper.createRequest(RequestCall.IEditProfile::class.java, header)
            call = requestCall.editProfile(userRequest)
            call!!.enqueue(this)
        }
    }

    override fun cancelRequest() {
        if (call != null) {
            call!!.cancel()
        }
    }
}