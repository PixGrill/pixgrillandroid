package com.pixgrill.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.pixgrill.model.response.UserResponse;


/**
 * Created by zoondia on 5/1/16.
 */
public class PreferenceData {
    private SharedPreferences sharedPreferences;
    private static final String PREFERENCE_NAME = "PIX_PREFERENCE";
    private static final String PREF_REFRESH_TOKEN = "REFRESH_TOKEN";
    private static final String PREF_USER_DATA = "USER_DATA";


    public PreferenceData(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }


    public void setRefreshToken(String registrationToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_REFRESH_TOKEN, registrationToken);
        editor.apply();

    }

    public String getRefreshToken() {
        return sharedPreferences.getString(PREF_REFRESH_TOKEN, null);

    }

    public void setUserDetails(UserResponse detailsModel) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        editor.putString(PREF_USER_DATA, gson.toJson(detailsModel));
        editor.apply();
    }

    public UserResponse getUserDetails() {
        String details = sharedPreferences.getString(PREF_USER_DATA, null);
        Gson gson = new Gson();
        UserResponse detailsModel = null;
        if (details != null)
            detailsModel = gson.fromJson(details, UserResponse.class);
        return detailsModel;
    }
}
