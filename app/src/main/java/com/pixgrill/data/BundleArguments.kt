package com.pixgrill.data

/**
 * Created by CarvreSeven on 23-02-2018 11:43 AM.
 */
object BundleArguments {

    const val ARGS_USER_ID = "ARGS_USER_ID"
    const val ARGS_POST_IMAGE_LIST = "ARGS_POST_IMAGE_LIST"
    const val ARGS_IS_FOLLOW = "ARGS_IS_FOLLOW"
    const val ARGS_FOLLOWER_OBJECT = "ARGS_FOLLOWER_OBJECT"
}
