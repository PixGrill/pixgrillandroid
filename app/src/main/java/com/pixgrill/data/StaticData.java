package com.pixgrill.data;

import android.content.Context;

import com.pixgrill.R;
import com.pixgrill.model.DrawerListModel;
import com.pixgrill.model.SheetListModel;

import java.util.ArrayList;
import java.util.HashMap;

import static com.pixgrill.data.Params.PARAM_CONTENT_TYPE;
import static com.pixgrill.data.Params.PARAM_HEADER_ACCESS_TOKEN;
import static com.pixgrill.data.Params.PARAM_HEADER_USER_ID;


public class StaticData {


    public static ArrayList<SheetListModel> getCameraPickerList() {
        ArrayList<SheetListModel> list = new ArrayList<>();
        list.add(new SheetListModel(0, "Gallery"));
        list.add(new SheetListModel(1, "Camera"));
        return list;
    }

    public static ArrayList<SheetListModel> getGenderPickerList() {
        ArrayList<SheetListModel> list = new ArrayList<>();
        list.add(new SheetListModel(1, "Male"));
        list.add(new SheetListModel(2, "Female"));
        list.add(new SheetListModel(0, "Other"));
        return list;
    }

    public static HashMap<String, String> getHeader(Context context) {
        PreferenceData data = new PreferenceData(context);
        HashMap<String, String> map = new HashMap<>();
        map.put(PARAM_HEADER_ACCESS_TOKEN, data.getRefreshToken());
        map.put(PARAM_HEADER_USER_ID, String.valueOf(data.getUserDetails().getUserId()));
        map.put(PARAM_CONTENT_TYPE, "application/json");
        return map;
    }
    public static ArrayList<DrawerListModel> getDrawerList() {
        ArrayList<DrawerListModel> list = new ArrayList<>();
        list.add(new DrawerListModel(R.drawable.ic_drawer_home, "Home"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_events, "Events"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_groups, "Workplace"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_wallet, "My Wallet"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_notifications, "Notifications"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_settings, "Settings"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_share, "Share App"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_faq, "FAQ"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_about, "About Us"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_privacy, "Privacy Policy"));
        list.add(new DrawerListModel(R.drawable.ic_drawer_logout, "Log Out"));
        return list;
    }

    public static ArrayList<SheetListModel> getCategoryList() {
        ArrayList<SheetListModel> list = new ArrayList<>();
        list.add(new SheetListModel(0, "Natural Photography"));
        list.add(new SheetListModel(1, "Wildlife Photography"));
        list.add(new SheetListModel(2, "Documentary photography"));
        list.add(new SheetListModel(3, "Fashion photography"));
        list.add(new SheetListModel(4, "Photojournalism"));
        list.add(new SheetListModel(5, "General Photography"));
        list.add(new SheetListModel(6, "Candid photography"));
        list.add(new SheetListModel(7, "Portrait Photography"));
        list.add(new SheetListModel(8, "Sports Photography"));
        list.add(new SheetListModel(9, "Architecture Photography"));
        list.add(new SheetListModel(10, "Others"));
        return list;
    }

    StaticData() {
        throw new RuntimeException("Stub!");
    }

    public static boolean isWhitespace(char ch) {
        throw new RuntimeException("Stub!");
    }

}