package com.pixgrill.data;

public interface Params {

    String PARAM_EMAIL = "email";
    String PARAM_PASSWORD = "password";
    String PARAM_DEVICE_TOKEN = "deviceToken";
    String PARAM_USER_ID = "userId";
    String PARAM_LOGIN_TYPE = "type";
    String PARAM_HEADER_USER_ID = "user_id";
    String PARAM_HEADER_ACCESS_TOKEN = "auth_token";
    String PARAM_CONTENT_TYPE = "Content-Type";



}