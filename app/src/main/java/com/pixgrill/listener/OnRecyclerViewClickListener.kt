package com.pixgrill.listener

interface OnRecyclerViewClickListener {
    fun onRecyclerViewClick(position: Int, clickedObject: Any )
}
