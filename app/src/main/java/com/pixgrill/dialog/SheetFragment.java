package com.pixgrill.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.pixgrill.R;
import com.pixgrill.adapter.SheetAdapter;
import com.pixgrill.widget.BottomSheetListView;

import java.util.ArrayList;

public class SheetFragment extends BottomSheetDialogFragment implements AdapterView.OnItemClickListener {
    private static final String ARGS_SHEET_LIST = "SHEET_LIST";
    private static final String ARGS_TITLE = "TITLE";
    private static final String ARGS_SHEET_ID = "SHEET_ID";
    private int mSheetId;
    private ArrayList<? extends Parcelable> mSheetList;
    private String mDialogTitle;
    private Listener listener;

    public SheetFragment() {
        // Required empty public constructor
    }

    public static SheetFragment newInstance(int dialogId, ArrayList<? extends Parcelable> sheetList, String tittle) {
        Bundle args = new Bundle();
        SheetFragment fragment = new SheetFragment();
        args.putParcelableArrayList(ARGS_SHEET_LIST, sheetList);
        args.putInt(ARGS_SHEET_ID, dialogId);
        args.putString(ARGS_TITLE, tittle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSheetList = getArguments().getParcelableArrayList(ARGS_SHEET_LIST);
            mSheetId = getArguments().getInt(ARGS_SHEET_ID);
            mDialogTitle = getArguments().getString(ARGS_TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_sheet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView(view);
    }

    private void initializeView(View view) {
        View header = getActivity().getLayoutInflater().inflate(R.layout.header_bottom_sheet_list, null);
        SheetAdapter adapter = new SheetAdapter(getActivity(), mSheetList);
        TextView textHeader = header.findViewById(R.id.textHeader);
        textHeader.setText(mDialogTitle);
        BottomSheetListView listBottomSheet = view.findViewById(R.id.listBottomSheet);
        listBottomSheet.addHeaderView(header);
        listBottomSheet.setAdapter(adapter);
        listBottomSheet.setOnItemClickListener(this);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (Exception e) {
            listener = (Listener) getTargetFragment();
            if(listener == null){
                listener = (Listener) getFragmentManager().getFragments().get(0);
            }
            e.printStackTrace();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (listener != null && id >= 0) {
            listener.itemStateSelected(adapterView.getAdapter().getItem(position), mSheetId);
        }
        dismiss();
    }


    public interface Listener {
        void itemStateSelected(Object sheetModel, int dialogId);
    }
}