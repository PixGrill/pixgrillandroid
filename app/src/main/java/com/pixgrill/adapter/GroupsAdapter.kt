package com.pixgrill.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.pixgrill.R
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.EventResponse

/**
 * Created by Developer on 1/2/2018.
 */
class GroupsAdapter(var _eventList: List<EventResponse>, val _listener: OnRecyclerViewClickListener, val context: Context) : RecyclerView.Adapter<GroupsAdapter.ListViewHolder>() {
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_eventList[position])
    }

    override fun getItemCount(): Int {
        return _eventList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.group_list_adapter, parent, false)
        return ListViewHolder(layoutInflator)
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {

        }

        override fun onClick(p0: View?) {
            _listener.onRecyclerViewClick(adapterPosition, _eventList[adapterPosition])
        }

//        var textViewLeagueName = itemView.textViewLeagueName


        fun bind(eventItemModel: EventResponse) {
//            textViewLeagueName.text = feedItemModel.strTeam
            itemView.setOnClickListener(this)
        }

    }


    fun filter(searchTeam: List<EventResponse>) {
        this._eventList = searchTeam
        notifyDataSetChanged()
    }

}