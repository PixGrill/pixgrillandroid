package com.pixgrill.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.pixgrill.R
import com.pixgrill.model.DrawerListModel

/**
 * Created by Developer on 12/14/2017.
 */
class HomeDrawerAdapter(val context: Context, private val sliderListItems: ArrayList<DrawerListModel>) : BaseAdapter() {
    private var ctx: Context = context
    private var items: ArrayList<DrawerListModel>? = sliderListItems

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        val view: View?
        val vh: ViewHolder

        if (convertView == null) {
            val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_drawer, parent, false)
            vh = ViewHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ViewHolder
        }

        vh.textViewMenu.text = items?.get(position)?.drawerName
        vh.textViewMenu.setCompoundDrawablesWithIntrinsicBounds(items?.get(position)?.drawerIcon!!, 0, 0, 0)
        return view
    }

    override fun getItem(position: Int): Any {
        return items?.get(position) ?: "No data"
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.size ?: 1
    }

    internal class ViewHolder(row: View?) {
        val textViewMenu: TextView = row?.findViewById(R.id.textViewMenu)!!
    }
}