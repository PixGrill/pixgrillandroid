package com.pixgrill.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pixgrill.R
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.WalletResponse

/**
 * Created by Developer on 1/2/2018.
 */
class WalletAdapter(var _walletList: List<WalletResponse>, val _listener: OnRecyclerViewClickListener, val context: Context) : RecyclerView.Adapter<WalletAdapter.ListViewHolder>() {
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_walletList[position])
    }

    override fun getItemCount(): Int {
        return _walletList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.wallet_list, parent, false)
        return ListViewHolder(layoutInflator)
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {

        }

        override fun onClick(p0: View?) {
            _listener.onRecyclerViewClick(adapterPosition, _walletList[adapterPosition])
        }

//        var textViewLeagueName = itemView.textViewLeagueName


        fun bind(walletItemModel: WalletResponse) {
//            textViewLeagueName.text = feedItemModel.strTeam
            itemView.setOnClickListener(this)
        }

    }


}