package com.pixgrill.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.pixgrill.R
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.NotificationModel

class NotificationAdapter (var _notificationList: ArrayList<NotificationModel>, val _listener: OnRecyclerViewClickListener, val context: Context) : RecyclerView.Adapter<NotificationAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.notification_list_adapter, parent, false)
        return ListViewHolder(layoutInflator)
    }

    override fun getItemCount(): Int {
        return _notificationList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_notificationList[position])
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var imageProfile: SimpleDraweeView?
        internal var textViewNickName: TextView?
        internal var textViewContent: TextView?
        internal var viewForeground: RelativeLayout?

        init {
            textViewNickName = itemView.findViewById(R.id.textViewNickName)
            textViewContent = itemView.findViewById(R.id.textViewContent)
            imageProfile = itemView.findViewById(R.id.imageProfile)
            viewForeground = itemView.findViewById(R.id.viewForeground)
        }

        override fun onClick(p0: View?) {
            _listener.onRecyclerViewClick(adapterPosition, _notificationList[adapterPosition])
        }

        fun bind(userModel: NotificationModel) {
            textViewNickName?.text = userModel?.nickName
            textViewContent?.text = userModel?.content
            imageProfile?.setImageURI(userModel?.image)
            itemView.setOnClickListener(this)
        }

    }
}