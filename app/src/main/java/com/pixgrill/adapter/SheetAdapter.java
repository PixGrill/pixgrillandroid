package com.pixgrill.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixgrill.R;
import com.pixgrill.model.SheetListModel;

import java.util.ArrayList;


public class SheetAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<? extends Parcelable> mStateList;

    public SheetAdapter(Context mContext, ArrayList<? extends Parcelable> mStateList) {
        this.mContext = mContext;
        this.mStateList = mStateList;
    }

    @Override
    public int getCount() {
        return mStateList.size();
    }

    @Override
    public Object getItem(int position) {
        return mStateList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = ((Activity) mContext).getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.list_bottom_sheet, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textListItem = convertView.findViewById(R.id.textListItem);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (mStateList.get(position) instanceof SheetListModel) {
            SheetListModel model = (SheetListModel) mStateList.get(position);
            viewHolder.textListItem.setText(model.getName());
        }
        return convertView;
    }

    private class ViewHolder {
        TextView textListItem;
    }
}
