package com.pixgrill.adapter

import android.support.v7.widget.RecyclerView
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import com.pixgrill.R

class PostDetailAdapter(var _postImageList: List<String>, val context: Context) : RecyclerView.Adapter<PostDetailAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.list_post_detail_adpter, parent, false)
        return ListViewHolder(layoutInflator)
    }

    override fun getItemCount(): Int {
        return _postImageList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_postImageList[position])
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var imagePost: SimpleDraweeView?

        init {
            imagePost = itemView.findViewById(R.id.imagePost)
        }

        override fun onClick(p0: View?) {
        }

        fun bind(s: String) {
            imagePost?.setImageURI(s)
        }

    }
}