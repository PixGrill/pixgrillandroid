package com.pixgrill.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.pixgrill.R
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.response.UserResponse

class SearchUserAdapter(var _userList: List<UserResponse>, val _listener: OnRecyclerViewClickListener, val context: Context) : RecyclerView.Adapter<SearchUserAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.user_list_adapter, parent, false)
        return ListViewHolder(layoutInflator)
    }

    override fun getItemCount(): Int {
        return _userList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_userList[position])
    }


    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var imageProfile: SimpleDraweeView?
        internal var textViewNickName: TextView?
        internal var textViewCategory: TextView?

        init {
            textViewNickName = itemView.findViewById(R.id.textViewNickName)
            textViewCategory = itemView.findViewById(R.id.textViewCategory)
            imageProfile = itemView.findViewById(R.id.imageProfile)
        }

        override fun onClick(p0: View?) {
            _listener.onRecyclerViewClick(adapterPosition, _userList[adapterPosition])
        }

        fun bind(userModel: UserResponse) {
            textViewNickName?.text = userModel?.nickName
            textViewCategory?.text = userModel?.category
            imageProfile?.setImageURI(userModel?.profileImageUrl)
            itemView.setOnClickListener(this)
        }

    }
}