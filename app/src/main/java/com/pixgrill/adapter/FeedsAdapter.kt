package com.pixgrill.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.pixgrill.R
import com.pixgrill.R.id.reportItem
import com.pixgrill.model.response.FeedResponse
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.ui.activity.comment.CommentActivity

/**
 * Created by Developer on 1/2/2018.
 */
class FeedsAdapter(var _feedList: List<FeedResponse>, val _listener: OnRecyclerViewClickListener, val context: Context) : RecyclerView.Adapter<FeedsAdapter.ListViewHolder>() {

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_feedList[position])
        holder?.buttonActionMore?.setOnClickListener{
            val popup = PopupMenu(context, it)
            popup.inflate(R.menu.post_popup_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.reportItem -> {
                        Toast(context)
                        true
                    }
                    R.id.blockItem -> {
                        true
                    }
                    R.id.hideItem -> {
                        true
                    }
                }
                false
            }
            popup.show()
        }
    }

    override fun getItemCount(): Int {
        return _feedList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.feed_list_adapter, parent, false)
        return ListViewHolder(layoutInflator)
    }


    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

        internal var checkboxComment: CheckBox?
        internal var buttonActionMore: ImageButton?
        internal var adView: AdView?

        init {
            checkboxComment = itemView.findViewById(R.id.checkboxComment)
            buttonActionMore = itemView.findViewById(R.id.buttonActionMore)
            adView = itemView.findViewById(R.id.adView)
        }

        override fun onClick(p0: View?) {
            _listener.onRecyclerViewClick(adapterPosition, _feedList[adapterPosition])
        }

//        var textViewLeagueName = itemView.textViewLeagueName


        fun bind(feedItemModel: FeedResponse) {
//            textViewLeagueName.text = feedItemModel.strTeam
            itemView.setOnClickListener(this)
            checkboxComment?.setOnClickListener {
                val intent = Intent(context, CommentActivity::class.java)
                context.startActivity(intent)
            }
            if(adapterPosition % 4 == 0){
                adView?.visibility = View.VISIBLE
                //adView?.adSize = AdSize.MEDIUM_RECTANGLE
                val adRequest = AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build()
                adView?.loadAd(adRequest)
            }else{
                adView?.visibility = View.GONE
            }

        }
    }

    fun filter(searchTeam: List<FeedResponse>) {
        this._feedList = searchTeam
        notifyDataSetChanged()
    }

}