package com.pixgrill.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pixgrill.R
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.CommentModel

class CommentsAdapter (var _commentsList: List<CommentModel>, val context: Context) : RecyclerView.Adapter<CommentsAdapter.ListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.comment_list_adapter, parent, false)
        return ListViewHolder(layoutInflator)
    }

    override fun getItemCount(): Int {
        return _commentsList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(_commentsList[position])
    }


    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var textViewNickName: TextView?
        internal var textViewComment: TextView?

        init {
            textViewNickName = itemView.findViewById(R.id.textViewNickName)
            textViewComment = itemView.findViewById(R.id.textViewComment)
        }

        override fun onClick(p0: View?) {
        }

        fun bind(commentItemModel: CommentModel) {
            textViewNickName?.text = commentItemModel.nickName
            textViewComment?.text = commentItemModel.comment
            itemView.setOnClickListener(this)
        }

    }
}