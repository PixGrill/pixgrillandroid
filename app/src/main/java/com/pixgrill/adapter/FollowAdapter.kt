package com.pixgrill.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.pixgrill.R
import com.pixgrill.listener.OnRecyclerViewClickListener
import com.pixgrill.model.FollowerModel

class FollowAdapter(var _followerList: List<FollowerModel>, val _listener: OnRecyclerViewClickListener, val context: Context) : RecyclerView.Adapter<FollowAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflator = LayoutInflater.from(context).inflate(R.layout.follow_list_adapter, parent, false)
        return ListViewHolder(layoutInflator)
    }

    override fun getItemCount(): Int {
        return _followerList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder?.bind(_followerList[position])
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var imageProfile: SimpleDraweeView?
        internal var textViewNickName: TextView?
        internal var textViewCategory: TextView?
        internal var textViewFollowed: TextView?
        internal var textViewFollow: TextView?
        internal var followContainer: LinearLayout?
        internal var followedContainer: LinearLayout?

        init {
            textViewNickName = itemView.findViewById(R.id.textViewNickName)
            textViewCategory = itemView.findViewById(R.id.textViewCategory)
            imageProfile = itemView.findViewById(R.id.imageProfile)
            textViewFollowed = itemView.findViewById(R.id.textViewFollowed)
            textViewFollow = itemView.findViewById(R.id.textViewFollow)
            followContainer = itemView.findViewById(R.id.followContainer)
            followedContainer = itemView.findViewById(R.id.followedContainer)
        }

        override fun onClick(p0: View?) {
            _listener.onRecyclerViewClick(adapterPosition, _followerList[adapterPosition])
        }

        fun bind(followerItemModel: FollowerModel) {
            textViewNickName?.text = followerItemModel.user?.nickName
            textViewCategory?.text = followerItemModel.user?.category
            imageProfile?.setImageURI(followerItemModel.user?.profileImageUrl)
            if (followerItemModel.isFollowerModel!!) {
                followedContainer?.visibility = View.VISIBLE
                followContainer?.visibility = View.GONE
            } else {
                followedContainer?.visibility = View.GONE
                followContainer?.visibility = View.VISIBLE
            }
            itemView.setOnClickListener(this)
        }

    }
}