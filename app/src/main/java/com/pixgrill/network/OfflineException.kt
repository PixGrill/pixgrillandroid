package com.pixgrill.network

import java.io.IOException


internal class OfflineException(message: String) : IOException(message)
