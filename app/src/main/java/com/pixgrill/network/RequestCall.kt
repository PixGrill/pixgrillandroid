package com.pixgrill.network


import com.pixgrill.base.ResponseBase
import com.pixgrill.model.UserRequestModel
import com.pixgrill.model.response.AppDataResponse
import com.pixgrill.model.response.UserResponse
import com.pixgrill.network.RequestURL.URL_APP_DATA
import com.pixgrill.network.RequestURL.URL_EDIT_PROFILE
import com.pixgrill.network.RequestURL.URL_LOGIN
import com.pixgrill.network.RequestURL.URL_SIGN_UP
import retrofit2.http.*


class RequestCall {

    interface IAppData {
        @FormUrlEncoded
        @POST(URL_APP_DATA)
        fun appData(@FieldMap fields: Map<String, String>): ErrorHandlingAdapter.RetroCall<ResponseBase<AppDataResponse>>
    }

    interface ILogin {
        @POST(URL_LOGIN)
        fun login(@Body body: UserRequestModel): ErrorHandlingAdapter.RetroCall<ResponseBase<UserResponse>>
    }

    interface ISignUp {
        @POST(URL_SIGN_UP)
        fun signUp(@Body body: UserRequestModel): ErrorHandlingAdapter.RetroCall<ResponseBase<UserResponse>>
    }

    interface IEditProfile {
        @POST(URL_EDIT_PROFILE)
        fun editProfile(@Body body: UserRequestModel): ErrorHandlingAdapter.RetroCall<ResponseBase<UserResponse>>
    }

}
