package com.pixgrill.network


object RequestURL {

    const val BASE_URL = "http://pixgrill.com/"

    const val URL_APP_DATA = "api/appData"

    const val URL_LOGIN = "api/login"

    const val URL_SIGN_UP = "api/register"

    const val URL_EDIT_PROFILE = "userapi/edit-profile"

}