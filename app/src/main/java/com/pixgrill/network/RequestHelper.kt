package com.pixgrill.network


import com.google.gson.GsonBuilder
import com.pixgrill.BuildConfig
import com.pixgrill.network.RequestURL.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RequestHelper {

    fun <S> createRequest(serviceClass: Class<S>, maps: HashMap<String, String>?): S {
        val builder = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(ErrorHandlingAdapter.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .setLenient()
                        .create()))

        val httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.cache(null)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder().header("Content-Type", "application/json")
                if (maps != null) {
                    for (map in maps) {
                        requestBuilder.addHeader(map.key, map.value)
                    }
                }
                chain.proceed(requestBuilder.build())
            }.addInterceptor(loggingInterceptor)
        } else {
            httpClient.addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder().header("Content-Type", "application/json")
                if (maps != null) {
                    for (map in maps) {
                        requestBuilder.addHeader(map.key, map.value)
                    }
                }
                chain.proceed(requestBuilder.build())
            }
        }
        val client = httpClient.build()

        val retrofit = builder.client(client).build()
        return retrofit.create(serviceClass)
    }


}
