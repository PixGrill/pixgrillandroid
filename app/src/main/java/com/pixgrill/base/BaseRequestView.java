package com.pixgrill.base;


public interface BaseRequestView<T,Q> extends BaseView<Q> {

    void onRequestStart();

    void onRequestStop();

    void onRequestError(String message);

    void onCriticalError();

    void onRequestSuccess(T response);
}
