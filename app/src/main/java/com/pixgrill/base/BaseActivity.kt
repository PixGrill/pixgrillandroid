package com.pixgrill.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.pixgrill.data.PreferenceData
import com.pixgrill.util.DisplayUtils
import com.pixgrill.util.ProgressUtils


abstract class BaseActivity : AppCompatActivity(),View.OnClickListener {


    protected val TAG = this.javaClass.simpleName
    protected lateinit var _progressUtils: ProgressUtils
    protected lateinit var _preferenceData: PreferenceData

    override fun onCreate(savedInstanceState: Bundle?) {
        window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        super.onCreate(savedInstanceState)
        _progressUtils = ProgressUtils(this)
        _preferenceData = PreferenceData(this)
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        onCreateViews(DisplayUtils(this))

    }
    override fun onClick(p0: View?) {
        onClickListener(p0);
    }
    open protected fun onCreateViews(displayUtils: DisplayUtils) {
        onViewCreated()
    }

    open protected fun onViewCreated() {

    }

    abstract  protected fun onClickListener(view: View?) ;
}
