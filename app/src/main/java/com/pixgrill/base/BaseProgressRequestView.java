package com.pixgrill.base;


public interface BaseProgressRequestView<T, Q, E> extends BaseView<Q> {

    void onRequestStart(E progressType);

    void onRequestStop(E progressType);

    void onRequestError(String message);

    void onCriticalError();

    void onRequestSuccess(T response);
}
