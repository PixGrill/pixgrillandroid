package com.pixgrill.base;

public  class ResponseBase<T> {
    private Boolean hasError;
    private Integer errorCode;
    private String message;
    private T response;

    public T getResponse() {
        return response;
    }

    public boolean isHasError() {
        return hasError;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
