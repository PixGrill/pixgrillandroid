package com.pixgrill.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.WindowManager
import com.pixgrill.data.PreferenceData

import com.pixgrill.util.ProgressUtils



open class BaseFragment : Fragment() {
    protected lateinit var _progressUtils: ProgressUtils
    protected lateinit var _preferenceData: PreferenceData
    protected val TAG = this.javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        super.onCreate(savedInstanceState)
        _progressUtils = ProgressUtils(this!!.activity!!)
        _preferenceData = PreferenceData(activity)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}
